#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "glut.h"

// dimensiunea ferestrei in pixeli
#define dim 550

#define NRITER_JF 5000// numarul maxim de iteratii pentru testarea apartenentei la mult.Julia-Fatou
#define NRITER_M 50//nr maxim iteratii pentru sirul pentru M

// modulul maxim pentru testarea apartenentei la mult.Julia-Fatou
#define MODMAX_JF 10000000
#define MODMAX_M 2 // Mandlerbrot caz special cu modulul maxim 2

// ratii ptr. CJuliaFatou
#define RX_JF 0.005
#define RY_JF 0.005

// ratii ptr. CM - cu cat este mai mica, cu atat densitatea punctelor desenate este mai mare (cu cat este mai mare, cu atat se observa backgroundul)
#define RX_M 0.005
#define RY_M 0.005

unsigned char prevKey;
int nivel = 0;


class CComplex {
public:
	CComplex() : re(0.0), im(0.0) {}
	CComplex(double re1, double im1) : re(re1 * 1.0), im(im1 * 1.0) {}
	CComplex(const CComplex &c) : re(c.re), im(c.im) {}
	~CComplex() {}

	CComplex &operator=(const CComplex &c)
	{
		re = c.re;
		im = c.im;
		return *this;
	}

	double getRe() const { return re; }
	void setRe(double re1) { re = re1; }

	double getIm() const { return im; }
	void setIm(double im1) { im = im1; }

	// c = x + Y * i => |c| = x^2 + y^2
	double getModul() const
	{
		return sqrt(re * re + im * im);
	}

	int operator==(CComplex &c1) const
	{
		return ((re == c1.re) && (im == c1.im));
	}

	// C = x + y*i. C^2 = (x^2 - y^2) + (2 * x * y) * i
	CComplex pow2() const
	{
		CComplex rez;
		rez.re = powl(re * 1.0, 2) - powl(im * 1.0, 2);
		rez.im = 2.0 * re * im;
		return rez;
	}

	friend CComplex operator+(const CComplex &c1, const CComplex &c2);
	friend CComplex operator*(CComplex &c1, CComplex &c2);

	void print(FILE *f) const
	{
		fprintf(f, "%.20f%+.20f i", re, im);
	}

private:
	double re, im;
};

CComplex operator+(const CComplex &c1, const CComplex &c2)
{
	CComplex rez(c1.re + c2.re, c1.im + c2.im);
	return rez;
}

CComplex operator*(CComplex &c1, CComplex &c2)
{
	CComplex rez(c1.re * c2.re - c1.im * c2.im,
		c1.re * c2.im + c1.im * c2.re);
	return rez;
}


class CJuliaFatou {
public:
	CJuliaFatou()
	{
		m.nriter = NRITER_JF;
		m.modmax = MODMAX_JF;
	}

	CJuliaFatou(CComplex &c)
	{
		m.c = c;
		m.nriter = NRITER_JF;
		m.modmax = MODMAX_JF;
	}

	~CJuliaFatou() {}

	void setmodmax(double v) { assert(v <= MODMAX_JF); m.modmax = v; }
	double getmodmax() const { return m.modmax; }

	void setnriter(int v) { assert(v <= NRITER_JF); m.nriter = v; }
	int getnriter() const { return m.nriter; }

	// testeaza daca x apartine multimii Julia-Fatou Jc (prin calcularea unui numar de termeni din sur si a vedea daca sirul pare sa convearga sau nu)
	// returneaza 0 daca apartine, -1 daca converge finit, +1 daca converge infinit
	int isIn(CComplex &x) const
	{
		int rez = 0;
		// tablou in care vor fi memorate valorile procesului iterativ z_n+1 = z_n * z_n + c
		CComplex z0, z1;

		z0 = x;
		for (int i = 1; i < m.nriter; i++)
		{
			z1 = z0 * z0 + m.c;
			if (z1 == z0)
			{
				// x nu apartine m.J-F deoarece procesul iterativ converge finit
				rez = -1;
				break;
			}
			if (z1.getModul() > m.modmax)
			{
				// x nu apartine m.J-F deoarece procesul iterativ converge la infinit
				rez = 1;
				break;
			}
			z0 = z1;
		}

		return rez;
	}

	// afisarea multimii J-F care intersecteaza multimea argument
	void display(double xmin, double ymin, double xmax, double ymax) const
	{
		glPushMatrix();// Save the current working matrix.
		glLoadIdentity();// Makes the new working matrix the identity.

						 //    glTranslated((xmin + xmax) * 1.0 / (xmin - xmax), (ymin + ymax)  * 1.0 / (ymin - ymax), 0); // translate the current identity
						 //    glScaled(1.0 / (xmax - xmin), 1.0 / (ymax - ymin), 1); // scale the current identity

						 // afisarea propriu-zisa
		glBegin(GL_POINTS);
		for (double x = xmin; x <= xmax; x += RX_JF)
			for (double y = ymin; y <= ymax; y += RY_JF)
			{
				CComplex z(x, y);
				int r = isIn(z);
				if (r == 0)
				{
					glVertex3d(x, y, 0);
				}
				else if (r == -1)
				{
					//fprintf(stdout, "   converge finit\n");
				}
				else if (r == 1)
				{
					//fprintf(stdout, "   converge infinit\n");
				}
			}
		fprintf(stdout, "STOP\n");
		glEnd();

		glPopMatrix();// Restore you previous matrix.
	}

protected:
	struct SDate {
		CComplex c;
		// nr. de iteratii
		int nriter;
		// modulul maxim
		double modmax;
	} m;
};


class CMandelbrot : public CJuliaFatou
{
public:
	CMandelbrot(CComplex &c)
		:CJuliaFatou(c)
	{
	}

	// testeaza daca x apartine multimii Mandelbrot (Julia-Fatou)  (prin calcularea unui numar de termeni din sir; daca acestia depasesc 2 in modul, converg spre infinite si nu fac parte din multime; altfel, fac parte)
	// returneaza -1 daca nu apartine, numere de la [1, nr_iteratii] care sugereaza cat de mult tinde la infinite sau nu
	int isIn(CComplex &x) const
	{
		int rez = 0;
		CComplex z0, z1;

		z0 = m.c;
		for (int i = 1; i < m.nriter; i++)
		{
			z1 = z0 * z0 + x;
			if (z1.getModul() > m.modmax)
			{
				// x nu apartine Mandelbrot.J-F deoarece procesul iterativ converge la infinit
				break;
			}
			z0 = z1;
			rez++;
		}

		return rez;
	}

	static void setColor(int reversedIntensity)
	{
		float r, g, b;
		float splitSize = 1.0 / NRITER_M; // split intoo NR_ITER equal parts

		float currentIntensity = reversedIntensity * splitSize; // this does white background
		currentIntensity = 1 - currentIntensity; // this reverts, makes black background

												 /*	reversedIntensity = [0... nr_iter);
												 reversedIntensity = 0 => converge spre infinite => black (0, 0, 0)
												 reversedIntensity = nr_iter => apartine total => white (1, 1, 1)
												 */

		// this does blue glowing
		r = g = currentIntensity;
		b = 2 * r > 1.0 ? 1.0 : 2 * r;

		//// this does green glowing
		//r = b = currentIntensity;
		//g = 2 * r > 1.0 ? 1.0 : 2 * r;

		//// this does yellow glowing
		//b = currentIntensity;
		//r = g = 2 * b > 1.0 ? 1.0 : 2 * b;

		//// this does red glowing
		//b = g = currentIntensity;
		//r = 2 * b > 1.0 ? 1.0 : 2 * b;

		glColor3f(r, g, b);
	}

	// afisarea multimii M.J-F care intersecteaza multimea argument
	void display(double xmin, double ymin, double xmax, double ymax) const
	{
		glPushMatrix();// Save the current working matrix.
		glLoadIdentity();// Makes the new working matrix the identity.
		glScaled(0.69, 0.69, 0.69); // scale the current identity (half)
		glTranslated(0.5, 0, 0);

		// afisarea propriu-zisa
		glBegin(GL_POINTS);
		for (double x = xmin; x <= xmax; x += RX_M)
			for (double y = ymin; y <= ymax; y += RY_M)
			{
				CComplex z(x, y);
				int r = isIn(z);

				setColor(r);
				glVertex3d(x, y, 0);
			}
		fprintf(stdout, "STOP\n");
		glEnd();

		glPopMatrix();// Restore previous matrix.
	}
};

// multimea Julia-Fatou pentru z0 = 0 si c = -0.12375 + 0.056805i
void Display8() {
	CComplex c(-0.12375, 0.056805);
	CJuliaFatou cjf(c);

	glColor3f(1.0, 0.1, 0.1);
	cjf.setnriter(30);
	cjf.display(-0.8, -0.4, 0.8, 0.4);
}

// multimea Julia-Fatou pentru z0 = 0 si c = -0.012 + 0.74i
void Display9() {
	CComplex c(-0.012, 0.74);
	CJuliaFatou cjf(c);

	glColor3f(1.0, 0.1, 0.1);
	cjf.setnriter(30);
	cjf.display(-1, -1, 1, 1);
}

// multimea Mandelbrot.Julia-Fatou pentru c = 0 si z = x, zn = zn-1 ^ 2 + c
void Displaym() {
	CComplex c(0, 0);
	CMandelbrot cm(c);

	cm.setnriter(NRITER_M);
	cm.setmodmax(MODMAX_M);
	cm.display(-2, -2, 2, 2);
}



class C2coord
{
public:
	C2coord()
	{
		m.x = m.y = 0;
	}

	C2coord(double x, double y)
	{
		m.x = x;
		m.y = y;
	}

	C2coord(const C2coord &p)
	{
		m.x = p.m.x;
		m.y = p.m.y;
	}

	C2coord &operator=(C2coord &p)
	{
		m.x = p.m.x;
		m.y = p.m.y;
		return *this;
	}

	int operator==(C2coord &p) const
	{
		return ((m.x == p.m.x) && (m.y == p.m.y));
	}

protected:
	struct SDate
	{
		double x, y;
	} m;
};


class CPunct : public C2coord
{
public:
	CPunct() : C2coord(0.0, 0.0)
	{}

	CPunct(double x, double y) : C2coord(x, y)
	{}

	CPunct &operator=(const CPunct &p)
	{
		m.x = p.m.x;
		m.y = p.m.y;
		return *this;
	}

	void getxy(double &x, double &y) const
	{
		x = m.x;
		y = m.y;
	}

	int operator==(CPunct &p) const
	{
		return ((m.x == p.m.x) && (m.y == p.m.y));
	}

	// nu e folosita nicaieri initial. folosit la fractalii care deseneaza punct cu punct
	void marcheaza() const
	{
		glBegin(GL_POINTS);
		glVertex2d(m.x, m.y);
		glEnd();
	}

	void print(FILE *fis) const
	{
		fprintf(fis, "(%+f,%+f)", m.x, m.y);
	}

};

// Pastreaza in interior un vector (de fapt doar coordonatele varfului vectorului, relativ la (0, 0) ceea ce ii dau directia vectorului.
class CVector : public C2coord
{
public:
	CVector() : C2coord(0.0, 0.0)
	{
		normalizare();
	}

	CVector(double x, double y) : C2coord(x, y)
	{
		normalizare();
	}

	CVector &operator=(CVector &p)
	{
		m.x = p.m.x;
		m.y = p.m.y;
		return *this;
	}

	int operator==(CVector &p) const
	{
		return ((m.x == p.m.x) && (m.y == p.m.y));
	}

	//transform originea din (0, 0) in p.x, p.y si returneaza punctul din varful noului vector (se schimba si lungimea)
	CPunct getDest(CPunct &orig, double lungime) const
	{
		double x, y;
		orig.getxy(x, y);
		CPunct p(x + m.x * lungime, y + m.y * lungime);
		return p;
	}

	// rotatie = transformare in radiani gradele cu care vrem sa rotim, pentru a putea aplica apoi formula cu sin / cos (de transformare polar-cartezian)
	void rotatie(double grade)
	{
		double x = m.x;
		double y = m.y;
		double pi = 4.0 * atan(1.0);
		double rad = pi * grade / 180.0; // simplificat prin 2

		m.x = x * cos(rad) - y * sin(rad);
		m.y = x * sin(rad) + y * cos(rad);
		normalizare();
	}

	// deseneaza = deseneaza vectorul (linia), transformand originea din (0, 0) in p.x, p.y
	void deseneaza(CPunct p, double lungime) const
	{
		double x, y;
		p.getxy(x, y);
		glColor3f(1.0, 0.1, 0.1);
		glBegin(GL_LINE_STRIP);
		glVertex2d(x, y);
		glVertex2d(x + m.x * lungime, y + m.y * lungime);
		glEnd();
	}

	void print(FILE *fis) const
	{
		fprintf(fis, "%+fi %+fj", C2coord::m.x, C2coord::m.y);
	}

private:
	// normalizare = vectorul este dat pt. directie, cu o lungime oarecare; normalizarea il transforma in vector unitate (lungime = 1) pentru a fi folosit apoi la desenarea cu o anumita lungime
	void normalizare()
	{
		double d = sqrt(m.x * m.x + m.y * m.y);
		if (d != 0.0)
		{
			m.x = m.x * 1.0 / d;
			m.y = m.y * 1.0 / d;
		}
	}
};


class CCurbaKoch
{
public:
	// fiecare segment se divizeaza in 4 segmente, pe care le desenez dupa unghiuri (intai apelez recursiv; fiecare segment se va desena la final, la nivel = 0)
	static void segmentKoch(double lungime, int nivel, CPunct &p, CVector v)
	{
		CPunct p1;
		if (nivel == 0)
		{
			v.deseneaza(p, lungime);
		}
		else
		{
			segmentKoch(lungime / 3.0, nivel - 1, p, v);

			p1 = v.getDest(p, lungime / 3.0);
			v.rotatie(60);
			segmentKoch(lungime / 3.0, nivel - 1, p1, v);

			p1 = v.getDest(p1, lungime / 3.0);
			v.rotatie(-120);
			segmentKoch(lungime / 3.0, nivel - 1, p1, v);

			p1 = v.getDest(p1, lungime / 3.0);
			v.rotatie(60);
			segmentKoch(lungime / 3.0, nivel - 1, p1, v);
		}
	}

	//  initial `desenez` trei laturi care formeaza triunghiul de la level = 0; pornesc recursia pe fiecare din ele. 
	static void afisare(double lungime, int nivel)
	{
		CVector v1(sqrt(3.0) / 2.0, 0.5);
		CPunct p1(-1.0, 0.0);

		CVector v2(0.0, -1.0);
		CPunct p2(0.5, sqrt(3.0) / 2.0);

		CVector v3(-sqrt(3.0) / 2.0, 0.5);
		CPunct p3(0.5, -sqrt(3.0) / 2.0);

		segmentKoch(lungime, nivel, p1, v1);
		segmentKoch(lungime, nivel, p2, v2);
		segmentKoch(lungime, nivel, p3, v3);
	}
};

class CArboreBinar
{
public:
	// la fiecare latura, desenez cate doua laturi, intre ele cu unghiu de 90grade; asta presupune intai rotire la -45 grade (sunt cu directia pe mijloc initial), iar apoi rotatie invers cu 90grade.
	static void arboreBinar(double lungime, int nivel, CPunct &p, CVector v)
	{
		CPunct p1;
		if (nivel == 0)
		{
			v.deseneaza(p, lungime);
		}
		else
		{
			arboreBinar(lungime, nivel - 1, p, v);
			p1 = v.getDest(p, lungime);

			v.rotatie(-45);
			arboreBinar(lungime / 2.0, nivel - 1, p1, v);

			v.rotatie(90);
			arboreBinar(lungime / 2.0, nivel - 1, p1, v);
		}
	}

	// initial desenez linia mare, apoi din ea incep si le adaug pe celelalte
	static void afisare(double lungime, int nivel)
	{
		CVector v(0.0, -1.0);
		CPunct p(0.0, 1.0);

		arboreBinar(lungime, nivel, p, v);
	}
};

class CArborePerron
{
public:
	/*
	* Structura repetitiva este:
	*   \/
	*   |_
	* \/
	* Obs: recursiv o sa merg doar din 4 din cele 6 ramuri (capetele)
	*/
	static void arborePerron(double lungime,
		int nivel,
		double factordiviziune,
		CPunct p,
		CVector v)
	{
		assert(factordiviziune != 0);
		if (nivel == 0)
			return;

		CPunct p1, temp;
		v.rotatie(30);
		v.deseneaza(p, lungime);
		p1 = v.getDest(p, lungime);
		arborePerron(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);

		v.rotatie(-90);
		v.deseneaza(p, lungime);
		p1 = v.getDest(p, lungime);
		temp = p1;

		v.rotatie(-30);
		v.deseneaza(p1, lungime);
		p1 = v.getDest(p1, lungime);
		arborePerron(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);

		p1 = temp;
		v.rotatie(90);
		v.deseneaza(p1, lungime);
		p1 = v.getDest(p1, lungime);
		temp = p1;

		v.rotatie(30);
		v.deseneaza(p1, lungime);
		p1 = v.getDest(p1, lungime);
		arborePerron(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);

		p1 = temp;
		v.rotatie(-90);
		v.deseneaza(p1, lungime);
		p1 = v.getDest(p1, lungime);
		arborePerron(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);
	}

	static void afisare(double lungime, int nivel)
	{
		CVector v(0.0, 1.0);
		CPunct p(0.0, -1.0);

		v.deseneaza(p, 0.25);
		p = v.getDest(p, 0.25);
		arborePerron(lungime, nivel, 0.4, p, v);
	}
};

class CCurbaHilbert
{
public:
	// Production rules Lindenmayer:
	//		- B F + A F A + F B -;
	//		where: - / + = rotate 90deg (with the switching of states when calling A or B), F = draw line, B = recursive style 1 (invert signs), A = recusrive style 2 (do not invert styles)
	// 
	// Production Alg. translated into drawings  / recursive:
	// (rotate -d * 90) (recursive2) (draw) -> (rotate +d * 90) (recursive1) (draw) (recursive1) -> (rotate +d * 90) (draw) (recursive2) (rotate -d * 90)
	// Observatii:
	//  - la noi, +90 de grade privit din fata inseamna de fapt desenare la -90 grade
	//  - recursive2 preesupune apelare recursiva cu semn schimbat
	//  - recursive1 presupune apelare recursiva cu semn neschimbat
	static void curbaHilbert(double lungime, int nivel, CPunct &p, CVector &v, int d)
	{
		if (nivel == 0)
			return;

		// (rotate d * 90)
		v.rotatie(d * 90);
		// (recursive2)
		curbaHilbert(lungime, nivel - 1, p, v, -d);
		// (draw)
		v.deseneaza(p, lungime);
		p = v.getDest(p, lungime);

		//(rotate -d * 90)
		v.rotatie(-d * 90);
		// (recursive1)
		curbaHilbert(lungime, nivel - 1, p, v, d);
		// (draw)
		v.deseneaza(p, lungime);
		p = v.getDest(p, lungime);
		// (recursive1)
		curbaHilbert(lungime, nivel - 1, p, v, d);

		// (rotate -d * 90)
		v.rotatie(-d * 90);
		// (draw)
		v.deseneaza(p, lungime);
		p = v.getDest(p, lungime);
		// (recursive2)
		curbaHilbert(lungime, nivel - 1, p, v, -d);
		// (rotate d * 90)
		v.rotatie(d * 90);
	}

	// start with 90 degrees
	static void afisare(double lungime, int nivel)
	{
		CVector v(0.0, 1.0);
		CPunct p(0.0, 0.0);

		curbaHilbert(lungime, nivel, p, v, 1);
	}
};

class CSquare
{
public:
	// la nivelul 0, algoritmul deseneaza patrate (nu doar linii)
	// pentru fiecare patrat desenat, merge recursiv si deseneaza cate 8 patrate in jurul lui, la coordonate relativ la coltul dreapta-sus, in functie de lungimea curenta
	static void segmentSquare(double lungime, int nivel, CPunct p, CVector v)
	{
		CPunct p1;
		if (nivel == 0)
		{
			v.deseneaza(p, lungime);
			p1 = v.getDest(p, lungime);
			v.rotatie(-90);
			v.deseneaza(p1, lungime);
			p1 = v.getDest(p1, lungime);
			v.rotatie(-90);
			v.deseneaza(p1, lungime);
			p1 = v.getDest(p1, lungime);
			v.rotatie(-90);
			v.deseneaza(p1, lungime);
		}
		else
		{
			double nl = lungime / 3.0;
			segmentSquare(lungime, nivel - 1, p, v);

			double x, y;
			p.getxy(x, y);

			// dreapta sus
			CPunct cp1(x + 2 * nl, y + 3 * nl);
			p1 = v.getDest(cp1, nl);
			segmentSquare(nl, nivel - 1, p1, v);

			// dreapta mijloc
			CPunct cp2(x + 2 * nl, y);
			p1 = v.getDest(cp2, nl);
			segmentSquare(nl, nivel - 1, p1, v);

			// dreapta jos
			CPunct cp3(x + 2 * nl, y - 3 * nl);
			p1 = v.getDest(cp3, nl);
			segmentSquare(nl, nivel - 1, p1, v);

			// jos - mijloc
			CPunct cp4(x - nl, y - 3 * nl);
			p1 = v.getDest(cp4, nl);
			segmentSquare(nl, nivel - 1, p1, v);

			// jos - stanga
			CPunct cp5(x - 4 * nl, y - 3 * nl);
			p1 = v.getDest(cp5, nl);
			segmentSquare(nl, nivel - 1, p1, v);

			// stanga - mijloc
			CPunct cp6(x - 4 * nl, y);
			p1 = v.getDest(cp6, nl);
			segmentSquare(nl, nivel - 1, p1, v);

			// stanga - sus
			CPunct cp7(x - 4 * nl, y + 3 * nl);
			p1 = v.getDest(cp7, nl);
			segmentSquare(nl, nivel - 1, p1, v);

			// sus - mijloc
			CPunct cp8(x - nl, y + 3 * nl);
			p1 = v.getDest(cp8, nl);
			segmentSquare(nl, nivel - 1, p1, v);
		}
	}

	static void afisare(double lungime, int nivel)
	{
		CVector v2(0, -1);//vert -> jos
		CPunct p2(0.15, 0.15); // origin

							   // desenare patrat mare
		glColor3f(1, 0, 0);
		glBegin(GL_QUADS);
		glVertex2f(0.45, 0.45);
		glVertex2f(-0.45, 0.45);
		glVertex2f(-0.45, -0.45);
		glVertex2f(0.45, -0.45);
		glEnd();

		segmentSquare(lungime, nivel, p2, v2);
	}
};

class CArboreCiudat
{
public:
	/*
	* Structura repetitiva este:
	* /\		(90deg)
	*	|\		(60deg, 15 stanga)
	*  /\		(120 deg, 30 stanga, mai scurte)
	* Obs: recursiv o sa merg doar din 4 din cele 6 ramuri (capetele)
	*/
	static void arboreCiudat(double lungime,
		int nivel,
		double factordiviziune,
		CPunct p,
		CVector v)
	{
		assert(factordiviziune != 0);
		if (nivel == 0)
			return;

		CPunct p1, temp;
		v.rotatie(-45);
		v.deseneaza(p, lungime);
		p1 = v.getDest(p, lungime);
		arboreCiudat(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);

		v.rotatie(90);
		v.deseneaza(p, lungime);
		p1 = v.getDest(p, lungime);
		temp = p1;

		v.rotatie(15);
		v.deseneaza(p1, lungime);
		p1 = v.getDest(p1, lungime);
		arboreCiudat(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);

		p1 = temp;
		v.rotatie(-60);
		v.deseneaza(p1, lungime);
		p1 = v.getDest(p1, lungime);
		temp = p1;

		v.rotatie(30);
		v.deseneaza(p1, lungime / 2);
		p1 = v.getDest(p1, lungime / 2);
		arboreCiudat(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);

		p1 = temp;
		v.rotatie(-120);
		v.deseneaza(p1, lungime / 2);
		p1 = v.getDest(p1, lungime / 2);
		arboreCiudat(lungime * factordiviziune, nivel - 1, factordiviziune, p1, v);
	}

	static void afisare(double lungime, int nivel)
	{
		CVector v(0.0, -1.0);
		CPunct p(-0.7, 2.0);

		v.deseneaza(p, 0.20);
		p = v.getDest(p, 0.20);
		arboreCiudat(lungime, nivel, 0.4, p, v);
	}
};

class CCurbaCiudata
{
public:
	// https://en.wikipedia.org/wiki/Sierpi%C5%84ski_arrowhead_curve
	// Sierpiński arrowhead curve
	// unghiul de rotatie: 60 de grade
	// Lindenmayer rules: X -> YF + XF + Y (starting with XF as base); Y = recursive sign changed, X = recursive sign keep, +/- (alternated according to X/Y) = rotations, F = draw
	//
	// Reguli traduse: +(revert angle) -> (recursive2) (draw) -> (d * 60) (recursive1) (draw) -> (d * 60) (recursive2)
	// recursive2: schimba semnul (d = -d)
	// recursive1: acelasi semn (d = d)
	// draw: only simulate draw (move the point), draw the actual lines at the end (need to build them all with the appropriate length; if we draw them sooner, we miss the required length when exiting the recursion and make them bigger - in Hilbert this worked because length never changed)
	// (revert angle) is needed to restart the position of the drawing (they somehow remain hanged +angle degrees)
	static void curbaCiudata(double lungime, int nivel, CPunct p, CVector v, int angle)
	{
		if (nivel == 0)
		{
			v.deseneaza(p, lungime);
		}
		else
		{
			v.rotatie(-angle);

			// (recursive2)
			curbaCiudata(lungime / 2.0, nivel - 1, p, v, -angle);
			// (draw)
			//v.deseneaza(p, lungime / 2.0);
			p = v.getDest(p, lungime / 2.0);

			// (d * 60)
			v.rotatie(angle);
			// (recursive1)
			curbaCiudata(lungime / 2.0, nivel - 1, p, v, angle);
			// (draw)
			//v.deseneaza(p, lungime / 2.0);
			p = v.getDest(p, lungime / 2.0);

			// (d * 60)
			v.rotatie(angle);
			// (recursive2)
			curbaCiudata(lungime / 2.0, nivel - 1, p, v, -angle);
		}
	}

	// start with 3 segments and draw from each one of them
	static void afisare(double lungime, int nivel)
	{
		/*
		* Start angle: 30 deg. (the closed part of the shape must be to the right).
		* Obs: it draws the shape in reverse-angle mode ( _ then | then \ )
		* _
		* _|
		*/
		CVector v1(sqrt(3.0) / 2.0, 0.50);
		CPunct p1(-0.90, -0.90);
		int angle = 60; // start with 60deg

		CPunct p2 = v1.getDest(p1, lungime);
		CVector v2(v1);
		v2.rotatie(angle);

		CPunct p3 = v2.getDest(p2, lungime);
		CVector v3(v2);
		v3.rotatie(angle);

		curbaCiudata(lungime, nivel, p1, v1, -angle);
		curbaCiudata(lungime, nivel, p2, v2, angle); // this segment builds the triangles opposed to the 1 and 3
		curbaCiudata(lungime, nivel, p3, v3, -angle);
	}
};

// afisare curba lui Koch "fulg de zapada"
void Display1() {
	CCurbaKoch::afisare(sqrt(3.0), nivel);

	char c[3];
	sprintf(c, "%2d", nivel);
	glRasterPos2d(-0.98, -0.98);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'N');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'v');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '=');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[0]);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[1]);

	glRasterPos2d(-1.0, 0.9);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'c');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'u');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'b');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ' ');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'u');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ' ');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'K');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'o');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'c');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'h');

	nivel++;
}

// afisare arbore binar
void Display2() {
	CArboreBinar::afisare(1, nivel);

	char c[3];
	sprintf(c, "%2d", nivel);
	glRasterPos2d(-0.98, -0.98);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'N');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'v');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '=');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[0]);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[1]);

	glRasterPos2d(-1.0, 0.9);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'b');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'o');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ' ');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'b');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'n');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');

	nivel++;
}

// afisare arborele lui Perron
void Display3() {
	char c[3];
	sprintf(c, "%2d", nivel);
	glRasterPos2d(-0.98, -0.98);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'N');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'v');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '=');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[0]);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[1]);

	glRasterPos2d(-1.0, -0.9);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'b');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'o');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ' ');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'P');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'o');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'n');

	// some scaling
	glPushMatrix();
	glLoadIdentity();
	glScaled(0.4, 0.4, 1);
	glTranslated(-0.5, -0.5, 0.0);
	CArborePerron::afisare(1, nivel);
	glPopMatrix();
	nivel++;
}

// afisare curba lui Hilbert
void Display4() {
	CCurbaHilbert::afisare(0.05, nivel);

	char c[3];
	sprintf(c, "%2d", nivel);
	glRasterPos2d(-0.98, -0.98);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'N');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'v');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '=');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[0]);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[1]);

	glRasterPos2d(-1.0, -0.9);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'c');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'u');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'b');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ' ');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'H');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'b');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 't');

	nivel++;
}

// afisare patratele
void Display5() {
	CSquare::afisare(0.3, nivel);



	char c[3];
	sprintf(c, "%2d", nivel);
	glRasterPos2d(-0.98, -0.98);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'N');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'v');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '=');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[0]);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[1]);

	glRasterPos2d(-1.0, -0.9);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'P');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 't');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 't');

	nivel++;
}

// afisare arborel
void Display6() {

	char c[3];
	sprintf(c, "%2d", nivel);
	glRasterPos2d(-0.98, -0.98);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'N');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'v');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '=');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[0]);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[1]);

	glRasterPos2d(-1.0, -0.9);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'A');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'b');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'o');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'r');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ' ');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'C');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'u');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'd');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'a');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 't');

	// some scaling
	glPushMatrix();
	glLoadIdentity();
	glScaled(0.4, 0.4, 1);
	//glTranslated(-0.5, -0.5, 0.0);
	CArboreCiudat::afisare(1, nivel);
	glPopMatrix();
	nivel++;
}


// afisare curba lui Hilbert
void Display7() {
	CCurbaCiudata::afisare(0.9, nivel);

	char c[3];
	sprintf(c, "%2d", nivel);
	glRasterPos2d(-0.98, -0.98);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'N');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'i');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'v');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'e');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, 'l');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, '=');
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[0]);
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c[1]);

	nivel++;
}

void Init(void) {

	glClearColor(1.0, 1.0, 1.0, 1.0);

	glLineWidth(1);

	//glPointSize(3); // making this bigger (leave it uncommented) makes the image pixelated (when resizing, on very closed-by points); commenting it makes the background color visible (points have spaces between them)

	glPolygonMode(GL_FRONT, GL_LINE);
}

void Display(void)
{
	switch (prevKey)
	{
	case '0':
		glClear(GL_COLOR_BUFFER_BIT);
		nivel = 0;
		fprintf(stderr, "nivel = %d\n", nivel);
		break;
	case '1':
		glClear(GL_COLOR_BUFFER_BIT);
		Display1();
		break;
	case '2':
		glClear(GL_COLOR_BUFFER_BIT);
		Display2();
		break;
	case '3':
		glClear(GL_COLOR_BUFFER_BIT);
		Display3();
		break;
	case '4':
		glClear(GL_COLOR_BUFFER_BIT);
		Display4();
		break;
	case '5':
		glClear(GL_COLOR_BUFFER_BIT);
		Display5();
		break;
	case '6':
		glClear(GL_COLOR_BUFFER_BIT);
		Display6();
		break;
	case '7':
		glClear(GL_COLOR_BUFFER_BIT);
		Display7();
		break;
	case '8':
		glClear(GL_COLOR_BUFFER_BIT);
		Display8();
		break;
	case '9':
		glClear(GL_COLOR_BUFFER_BIT);
		Display9();
		break;
	case 'm':
		glClear(GL_COLOR_BUFFER_BIT);
		Displaym();
		break;
	default:
		break;
	}

	glFlush();
}


void Reshape(int w, int h)
{
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);
}

void KeyboardFunc(unsigned char key, int x, int y)
{
	prevKey = key;
	if (key == 27) // escape
		exit(0);
	glutPostRedisplay();
}

void MouseFunc(int button, int state, int x, int y)
{
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitWindowSize(dim, dim);

	glutInitWindowPosition(100, 100);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutCreateWindow("HW5");

	Init();

	glutReshapeFunc(Reshape);

	glutKeyboardFunc(KeyboardFunc);

	glutMouseFunc(MouseFunc);

	glutDisplayFunc(Display);

	glutMainLoop();

	return 0;
}
