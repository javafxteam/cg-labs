#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "glut.h"

// dimensiunea ferestrei in pixeli
#define dim 550

#define NRITER_JF 5000// numarul maxim de iteratii pentru testarea apartenentei la mult.Julia-Fatou
#define NRITER_M 50//nr maxim iteratii pentru sirul pentru M

// modulul maxim pentru testarea apartenentei la mult.Julia-Fatou
#define MODMAX_JF 10000000
#define MODMAX_M 2 // Mandlerbrot caz special cu modulul maxim 2

// ratii ptr. CJuliaFatou
#define RX_JF 0.005
#define RY_JF 0.005

// ratii ptr. CM - cu cat este mai mica, cu atat densitatea punctelor desenate este mai mare (cu cat este mai mare, cu atat se observa backgroundul)
#define RX_M 0.005
#define RY_M 0.005

unsigned char prevKey;

class CComplex {
public:
	CComplex() : re(0.0), im(0.0) {}
	CComplex(double re1, double im1) : re(re1 * 1.0), im(im1 * 1.0) {}
	CComplex(const CComplex &c) : re(c.re), im(c.im) {}
	~CComplex() {}

	CComplex &operator=(const CComplex &c) 
	{
		re = c.re;
		im = c.im;
		return *this;
	}

	double getRe() const {return re;}
	void setRe(double re1) {re = re1;}
  
	double getIm() const {return im;}
	void setIm(double im1) {im = im1;}

	// c = x + Y * i => |c| = x^2 + y^2
	double getModul() const
	{
		return sqrt(re * re + im * im);
	}

	int operator==(CComplex &c1) const
	{
		return ((re == c1.re) && (im == c1.im));
	}

	// C = x + y*i. C^2 = (x^2 - y^2) + (2 * x * y) * i
	CComplex pow2() const
	{
		CComplex rez;
		rez.re = powl(re * 1.0, 2) - powl(im * 1.0, 2);
		rez.im = 2.0 * re * im;
		return rez;
	}

	friend CComplex operator+(const CComplex &c1, const CComplex &c2);
	friend CComplex operator*(CComplex &c1, CComplex &c2);

	void print(FILE *f) const
	{
		fprintf(f, "%.20f%+.20f i", re, im);
	}

private:
	double re, im;
};

CComplex operator+(const CComplex &c1, const CComplex &c2) 
{
	CComplex rez(c1.re + c2.re, c1.im + c2.im);
	return rez;
}

CComplex operator*(CComplex &c1, CComplex &c2) 
{
	CComplex rez(c1.re * c2.re - c1.im * c2.im,
                c1.re * c2.im + c1.im * c2.re);
	return rez;
}


class CJuliaFatou {
public:
  CJuliaFatou()
  {
    m.nriter = NRITER_JF;
    m.modmax = MODMAX_JF;
  }
  
  CJuliaFatou(CComplex &c)
  {
    m.c = c;
    m.nriter = NRITER_JF;
    m.modmax = MODMAX_JF;
  }
  
  ~CJuliaFatou() {}

  void setmodmax(double v) {assert(v <= MODMAX_JF); m.modmax = v;}
  double getmodmax() const {return m.modmax;}

  void setnriter(int v) {assert(v <= NRITER_JF); m.nriter = v;}
  int getnriter() const {return m.nriter;}

  // testeaza daca x apartine multimii Julia-Fatou Jc (prin calcularea unui numar de termeni din sur si a vedea daca sirul pare sa convearga sau nu)
  // returneaza 0 daca apartine, -1 daca converge finit, +1 daca converge infinit
  int isIn(CComplex &x) const
  {
    int rez = 0;
    // tablou in care vor fi memorate valorile procesului iterativ z_n+1 = z_n * z_n + c
    CComplex z0,z1;

	z0 = x;
	for (int i = 1; i < m.nriter; i++)
	{
		z1 = z0 * z0 + m.c;
		if (z1 == z0) 
		{
			// x nu apartine m.J-F deoarece procesul iterativ converge finit
			rez = -1;
			break;
		}
		if (z1.getModul() > m.modmax)
		{
			// x nu apartine m.J-F deoarece procesul iterativ converge la infinit
			rez = 1;
			break;
		}
		z0 = z1;
	}

    return rez;
  }

  // afisarea multimii J-F care intersecteaza multimea argument
  void display(double xmin, double ymin, double xmax, double ymax) const
  {
	glPushMatrix();// Save the current working matrix.
	glLoadIdentity();// Makes the new working matrix the identity.

//    glTranslated((xmin + xmax) * 1.0 / (xmin - xmax), (ymin + ymax)  * 1.0 / (ymin - ymax), 0); // translate the current identity
//    glScaled(1.0 / (xmax - xmin), 1.0 / (ymax - ymin), 1); // scale the current identity
    
  	// afisarea propriu-zisa
    glBegin(GL_POINTS); 
    for(double x = xmin; x <= xmax; x += RX_JF)
		for(double y = ymin; y <= ymax; y += RY_JF) 
		{
			CComplex z(x, y);
			int r = isIn(z);
			if (r == 0) 
			{
				glVertex3d(x, y, 0);
			}
			else if (r == -1)
			{
				//fprintf(stdout, "   converge finit\n");
			}
			else if (r == 1)
			{
				//fprintf(stdout, "   converge infinit\n");
			}
		}
    fprintf(stdout, "STOP\n");
    glEnd();

    glPopMatrix();// Restore you previous matrix.
  }

protected:
  struct SDate {
    CComplex c;
    // nr. de iteratii
    int nriter;
    // modulul maxim
    double modmax;
  } m;
};


class CMandelbrot: public CJuliaFatou
{
public:
	CMandelbrot(CComplex &c)
		:CJuliaFatou(c)
	{
	}

	// testeaza daca x apartine multimii Mandelbrot (Julia-Fatou)  (prin calcularea unui numar de termeni din sir; daca acestia depasesc 2 in modul, converg spre infinite si nu fac parte din multime; altfel, fac parte)
	// returneaza -1 daca nu apartine, numere de la [1, nr_iteratii] care sugereaza cat de mult tinde la infinite sau nu
	int isIn(CComplex &x) const
	{
		int rez = 0;
		CComplex z0, z1;
		
		z0 = m.c;
		for (int i = 1; i < m.nriter; i++)
		{
			z1 = z0 * z0 + x;
			if (z1.getModul() > m.modmax)
			{
				// x nu apartine Mandelbrot.J-F deoarece procesul iterativ converge la infinit
				break;
			}
			z0 = z1;
			rez++;
		}

		return rez;
	}

	static void setColor(int reversedIntensity)
	{
		float r, g, b;
		float splitSize = 1.0 / NRITER_M; // split intoo NR_ITER equal parts

		float currentIntensity = reversedIntensity * splitSize; // this does white background
		currentIntensity = 1 - currentIntensity; // this reverts, makes black background

		/*	reversedIntensity = [0... nr_iter);
			reversedIntensity = 0 => converge spre infinite => black (0, 0, 0)
			reversedIntensity = nr_iter => apartine total => white (1, 1, 1)
		*/

		// this does blue glowing
		r = g = currentIntensity;
		b = 2 * r > 1.0 ? 1.0 : 2 * r;

		//// this does green glowing
		//r = b = currentIntensity;
		//g = 2 * r > 1.0 ? 1.0 : 2 * r;

		//// this does yellow glowing
		//b = currentIntensity;
		//r = g = 2 * b > 1.0 ? 1.0 : 2 * b;

		//// this does red glowing
		//b = g = currentIntensity;
		//r = 2 * b > 1.0 ? 1.0 : 2 * b;

		glColor3f(r, g, b);
	}

	// afisarea multimii M.J-F care intersecteaza multimea argument
	void display(double xmin, double ymin, double xmax, double ymax) const
	{
		glPushMatrix();// Save the current working matrix.
		glLoadIdentity();// Makes the new working matrix the identity.
		glScaled(0.69, 0.69, 0.69); // scale the current identity (half)
		glTranslated(0.5, 0, 0);

		// afisarea propriu-zisa
		glBegin(GL_POINTS);
		for (double x = xmin; x <= xmax; x += RX_M)
			for (double y = ymin; y <= ymax; y += RY_M)
			{
				CComplex z(x, y);
				int r = isIn(z);

				setColor(r);
				glVertex3d(x, y, 0);
			}
		fprintf(stdout, "STOP\n");
		glEnd();

		glPopMatrix();// Restore previous matrix.
	}
};

// multimea Julia-Fatou pentru z0 = 0 si c = -0.12375 + 0.056805i
void Display1() {
  CComplex c(-0.12375, 0.056805);
  CJuliaFatou cjf(c);

  glColor3f(1.0, 0.1, 0.1);
  cjf.setnriter(30);
  cjf.display(-0.8, -0.4, 0.8, 0.4);
}

// multimea Julia-Fatou pentru z0 = 0 si c = -0.012 + 0.74i
void Display2() {
  CComplex c(-0.012, 0.74);
  CJuliaFatou cjf(c);

  glColor3f(1.0, 0.1, 0.1);
  cjf.setnriter(30);
  cjf.display(-1, -1, 1, 1);
}

// multimea Mandelbrot.Julia-Fatou pentru c = 0 si z = x, zn = zn-1 ^ 2 + c
void Display3() {
	CComplex c(0, 0);
	CMandelbrot cm(c);

	glColor3f(1.0, 0, 0);
	cm.setnriter(NRITER_M);
	cm.setmodmax(MODMAX_M);
	cm.display(-2, -2, 2, 2);
}

void Init() {

   glClearColor(1.0, 1.0, 1.0, 0.0);

   glLineWidth(1);

   //glPointSize(3); // making this bigger (leave it uncommented) makes the image pixelated (when resizing, on very closed-by points); commenting it makes the background color visible (points have spaces between them)

   glPolygonMode(GL_FRONT, GL_LINE);

   glShadeModel(GL_SMOOTH);
}

void Display(void) {

  glClearColor(1.0, 1.0, 1.0, 1.0);
  switch(prevKey) {
  case '1':
    glClear(GL_COLOR_BUFFER_BIT);
    Display1();
    break;
  case '2':
    glClear(GL_COLOR_BUFFER_BIT);
    Display2();
    break;
  case '3':
	  glClear(GL_COLOR_BUFFER_BIT);
	  Display3();
	  break;
  default:
    break;
  }

  glFlush();
}

void Reshape(int w, int h) {
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	float ratio = 1.0 * w / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45, ratio, 1, 1000);
}

void KeyboardFunc(unsigned char key, int x, int y) {
   prevKey = key;
   if (key == 27) // escape
      exit(0);
   glutPostRedisplay();
}

void MouseFunc(int button, int state, int x, int y) {
}

int main(int argc, char** argv) {

   glutInit(&argc, argv);
   
   glutInitWindowSize(dim, dim);

   glutInitWindowPosition(100, 100);

   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

   glutCreateWindow (argv[0]);

   Init();

   glutReshapeFunc(Reshape);
   
   glutKeyboardFunc(KeyboardFunc);
   
   glutMouseFunc(MouseFunc);

   glutDisplayFunc(Display);
   
   glutMainLoop();

   return 0;
}


