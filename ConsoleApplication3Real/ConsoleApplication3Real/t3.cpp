// Daca se doreste utilizarea bibliotecii GLUT trebuie
// inclus fisierul header GL/glut.h (acesta va include
// la GL/gl.h si GL/glu.h, fisierele header pentru
// utilizarea bibliotecii OpenGL). Functiile din biblioteca
// OpenGL sunt prefixate cu gl, cele din GLU cu glu si
// cele din GLUT cu glut.

// #include <GL/glut.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include<vector>

#include "glut.h"

#define W_WIDTH 500

// represents how much of the viewport should I draw in (0 - 2)
const double MAX_VIEWPORT_S_SIZE = 1.9;
const float PI = atan(1.0) * 4.0;

unsigned char prevKey;

enum Colors{
	Red, Blue, Green, Black, Silver, White
};

class Color
{
public:
	float R, G, B;

	Color()
	{
		Init(0, 0, 0);
	}

	Color(float r, float g, float b)
	{
		Init(r, g, b);
	}

	Color(Colors color)
	{
		switch (color)
		{
		case Red:
			Init(1, 0, 0);
			break;
		case Blue:
			Init(0, 0, 1);
			break;
		case Green:
			Init(0, 1, 0);
			break;
		case Black:
			Init(0, 0, 0);
			break;
		case Silver:
			Init(0.75, 0.75, 0.75);
			break;
		default:
			Init(1, 1, 1);
			break;
		}
	}

	void Init(float r, float g, float b)
	{
		R = r;
		G = g;
		B = b;
	}
};

class Pixel
{
public:
	float x, y;
	int i, j;
	bool isLighted;
	Color* color;
	

	Pixel()
	{
		Init(0, 0, 0, 0, false, new Color());
	}

	Pixel(const Pixel& pixel)
	{
		this->i = pixel.i;
		this->j = pixel.j;
		this->x = pixel.x;
		this->y = pixel.y;
		this->isLighted = pixel.isLighted;
		this->color = pixel.color;
	}

	Pixel(int i, int j, float x, float y, Color* color)
	{
		Init(i, j, x, y, false, color);
	}

	Pixel(int i, int j, float x, float y, bool isLighted, Color* color)
	{
		Init(i,  j, x, y, isLighted, color);
	}

	void Init(int i, int j, float x, float y, bool isLighted, Color* color)
	{
		this->i = i;
		this->j = j;
		this->x = x;
		this->y = y;
		this->isLighted = isLighted;
		this->color = color;
	}

	float GetDistToPoint(float x, float y) const
	{
		return sqrt(pow(this->x - x, 2) + pow(this->y - y, 2));
	}

	// Draws this pixel represented as a Circle (uses polygons, which if given a big enough number of segments, form a `circle`)
	void Draw(float scaleX, float scaleY, float r, int num_segments = 24)
	{
		isLighted = true;
		glColor3f(color->R, color->G, color->B);
		glBegin(GL_POLYGON);
		for (int i = num_segments - 1; i >= 0; i--) // reverse order of drawing to fill it (CCW for GL_FRONT)
		{
			float theta = 2.0f * PI * float(i) / float(num_segments);//get the current angle

																	 // trebuie sa scalam si coordonatele cercului creat pentru a-i mentine aspect-ratio-ul 1:1 (ca si la patrate)
			float nx = r * cosf(theta) * scaleX;//compute the x component
			float ny = r * sinf(theta) * scaleY;//compute the y component

			glVertex2f(nx + x, ny + y);//output vertex

		}
		glEnd();
		glFlush();// force redraw
	}

	void DrawLineTo(Pixel* p2, Color* color) const
	{
		// draw the actual line too
		glColor3f(color->R, color->G, color->B);
		glBegin(GL_LINES);
			glVertex2f(x, y);
			glVertex2f(p2->x, p2->y);
		glEnd();
		glFlush();// force redraw
	}
};


class GrilaCarteziana
{
	int N; // NR. COLOANE
	int M; // NR. LINII
	double maxWidth; // starts with MAX_VIEWPORT_S_SIZE and represents the maximum width of the GRID as represented in viewport-size length [0, 2]
	double maxHeight;
	int wWidth, wHeight; // width/height of the original window (the space to draw on)
	double sWidth, sHeight; // width / height of a single square drawn
	const float pixelPercent = 0.3; // represents % the pixel is compared to the square size 
	Color* pixelColor; // specifies the default color for pixels
	bool pixelsExist = false; // specifies if we already created the pixels, or it is our first time

public:
	Pixel** pixels; // the array keeping the pixels
	float pixelRadius; // specified the radius of the drawn pixels
	float scaleX; // represents (on X-Axis) the % (so it is in [0,1]) of hou much should be scaled to keep the aspect ratio of the squares of 1:1
	float scaleY; // represents (on Y-Axis) the % (so it is in [0,1]) of how much should be scaled to keep the aspect ratio of the squares of 1:1

	// Inits a Grid with N columns and M lines (to keep the xOy-like coordinate system)
	// pixels[x][y] represents pixels on line y and column x
	GrilaCarteziana(int n, int m)
	{
		// need N/M squares, so, add 1
		N = n + 1;
		M = m + 1;
		printf("Init matrix of %dx%d.\n", N, M);
		pixelColor = new Color(Green);

		InitOnRedraw();
	}

	Pixel* GetPixel(int i, int j) const
	{
		if (i < 0 || i > N || j < 0 || j > M)
			return nullptr;
		return &pixels[i][j];
	}

	void InitOnRedraw()
	{
		// set defaults
		maxWidth = MAX_VIEWPORT_S_SIZE;
		maxHeight = MAX_VIEWPORT_S_SIZE;

		// init proportions / array
		SetWindowSize();
		DecideProportions();
		InitPixelArray();
	}

	void SetWindowSize()
	{
		// get width / height
		int cHeight = glutGet(GLUT_WINDOW_HEIGHT);
		int cWidth = glutGet(GLUT_WINDOW_WIDTH);
		wWidth = cWidth;
		wHeight = cHeight;
		
		// get the maximum possible (from MIN width/height) square size
		// we then scale X and Y according to this Square (to keep 1:1 aspect ratio)
		int minSize = cWidth < cHeight ? cWidth : cHeight;

		// get scale factor compared to 1:1 (square) aspect ratio for x and y [they are both in [0,1]]
		scaleX = float(cWidth) / minSize;
		scaleY = float(cHeight) / minSize;
		
		// if we make the window larger than the minSize, we need to invert the scaling (scaleX / scaleY will be  > 1, they need to be <= 1)
		// we do this cause we need to remain in [0,1] so the maxWidth and maxHeight keep their proportions
		// !! (scaleX in [0,1] => maxWidth in [0, MAX_VIEWPORT_S_SIZE])
		if (cWidth > minSize)
			scaleX = 1.0 / scaleX;
		if (cHeight > minSize)
			scaleY = 1.0 / scaleY;

		// compute the new maxWidth and maxHeight (keep them square, relative to the viewport scaling)
		// Why?? If window WIDTH is bigger than it's HEIGHT => 0.1 in width (on viewport) > 0.1 on height(on viewport)
		// so we adjust them to have different values, but the aprox. same size
		maxWidth = maxWidth * scaleX;
		maxHeight = maxHeight * scaleY;

		printf("Updated width (%d) and height (%d).\n", cWidth, cHeight);
		printf("Scale factor (compared to 1:1) X (%f) and Y (%f).\n", scaleX, scaleY);
		printf("Updated MAXwidth (%f) and MAXheight (%f).\n", maxWidth, maxHeight);
	}

	void DecideProportions()
	{
		// gets the maximum number of squares we need to compute and the square length (window-based-measure)
		int maxSize = N > M ? N : M;
		int minWindowSize = wWidth < wHeight ? wWidth : wHeight;
		float partSizeUnscaled = minWindowSize / float(maxSize);

		/*
		 * We transpose the size in window-length TO size in viewport-length. We then multiply with maxWidth / maxHeight to adjust the aspect ratio to X/Y axis (see SetWindowSize)
		 * Regula 3-simpla pentru raportat de la window-width la viewport-width (pentru ca initial latimea/inaltimea patratulului le calculez impartind dimensiunea ferestrei in N bucati egale)
		 * viewportMax (maxWidth)    ............. X (squareSize)
		 * windowMax (minWindowSize) ............. w (squareSize Unscaled)
		 * => X = viewportMax * ( sizeUnscaled / windowMax) (we choose asa windowMax the minimum length between width and height)
		 */
		sWidth = maxWidth * (partSizeUnscaled / minWindowSize); // getSquareWidth in Viewport size
		sHeight = maxHeight * (partSizeUnscaled / minWindowSize); // getSquareHeight in Viewport size
		
		// compute pixel radius with the given percent of a square
		float minSquareSize = sWidth < sHeight ? sWidth : sHeight;
		pixelRadius = pixelPercent * minSquareSize;

		printf("Computed square width (%f) and height (%f).\n", sWidth, sHeight);
		printf("Pixel radius will be (%f).\n", pixelRadius);

		// adjust local MAX_WIDTH and MAX_HEIGHT so when i draw => they are ~centered around 0
		maxWidth = N * sWidth;
		maxHeight = M * sHeight;
	}

	void InitPixelArray()
	{
		if (!pixelsExist)
		{
			pixelsExist = true;
			pixels = new Pixel*[N];
			for (int i = 0; i < N; i++)
				pixels[i] = new Pixel[M];
		}

		// compute them centered in (0, 0) from the MAX_HEIGHT/WIDTH
		// coordinates will be from left-bottom -> right-top
		int i = 0, j;
		for (float pozx = -maxWidth / 2 + sWidth / 2; i < N; pozx += sWidth) // start not too further down nor too further up (eg. divide squuare height by 2 and start from there)
		{
			j = 0;
			for (float pozy = -maxHeight / 2 + sHeight / 2; j < M; pozy += sHeight)
			{
				// keep coloor / lightining if it existed
				bool isLighted = false;
				Color* color = pixelColor;
				if (pixelsExist)
				{
					isLighted = pixels[i][j].isLighted;
					color = pixels[i][j].color;
				}
				Pixel* pixie = new Pixel(i, j, pozx, pozy, isLighted, color);
				pixels[i][j] = *pixie;
				j++;
			}
			i++;
		}
	}

	void ClearAllPixels() const
	{
		if (!pixelsExist)
		{
			printf("Trying to clear the matrix of pixels while it does not exist.\n");
			return;
		}

		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				pixels[i][j].isLighted = false;
	}

	// Lights up a pixel at the given position in the pixel matrix
	void LightUpPixelFromMatrix(int x, int y, Colors color) const
	{
		if(!pixelsExist)
		{
			printf("Trying to light up a pixel when it does not exist. Coordinates: (%d, %d).\n", x, y);
			return;
		}
		pixels[x][y].isLighted = true;
		pixels[x][y].color = new Color(color);
		pixels[x][y].Draw(scaleX, scaleY, pixelRadius);
	}

	// Lights up a pixel at the given position in the pixel matrix
	void LightUpPixelFromCoordinates(float x, float y, Colors color) const
	{
		Pixel* oldPixel = nullptr;
		bool found = false;
		for (int i = 0; i < N && !found; i++)
		{
			for (int j = 0; j < M && !found; j++)
				if (pixels[i][j].x == x && pixels[i][j].y == y)
				{
					oldPixel = &pixels[i][j];
					found = true;
				}
		}

		if (!found)
		{
			printf("Trying to light up a pixel when it does not exist. Coordinates: (%f, %f).\n", x, y);
			return;
		}

		LightUpPixelFromMatrix(oldPixel->i, oldPixel->j, color);
	}

	// Maps a point from [(0,0) - (w,h)] to [(-1,-1] - (1,1)]
	void TransformPointFromWindowToPointFromViewport(float& cx, float& cy) const
	{
		// get half of width and half of height (the Unity - it has size 1 in Viewport)
		float hWidth = float(wWidth) / 2;
		float hHeight = float(wHeight) / 2;

		// compute coordinates relative to the middle and the 4 quadrants (regarding the sign)
		float xHalfed = cx >= hWidth ? cx - hWidth : -(hWidth - cx);
		float yHalfed = cy <= hHeight ? (hHeight - cy) : -(cy - hHeight);

		// The middle represents the length of 1, so to get the actual coordinate, divide
		xHalfed = xHalfed / hWidth;
		yHalfed = yHalfed / hHeight;

		printf("Computed point coordinate: (%f, %f) --> (%f, %f).\n", cx, cx, xHalfed, yHalfed);
		cx = xHalfed;
		cy = yHalfed;
	}

	// given the window-size coordinates (x,y) return the pixel lying there (if there is one, in the pixelRadius clicked)
	Pixel* GetClosestPixel(int x, int y) const
	{
		float cx = x, cy = y;
		TransformPointFromWindowToPointFromViewport(cx, cy);

		float minDist = INT_MAX;
		Pixel* oldPixel = nullptr;
		bool found = false;
		for(int i = 0; i < N && !found; i++)
			for(int j = 0; j < M && !found; j++)
			{
				float pDist = pixels[i][j].GetDistToPoint(cx, cy);
				if(pDist < minDist && pDist <= pixelRadius)
				{
					minDist = pDist;
					oldPixel = &pixels[i][j];
					found = true;
				}
			}

		if(found)
		{
			printf("Got closest pixel at (%f, %f) in the matrix at (%d, %d).\n", oldPixel->x, oldPixel->y, oldPixel->i, oldPixel->j);
		}

		return oldPixel;
	}

	// Draws the Grid and the lighted pixels
	void Draw() const
	{
		glClear(GL_COLOR_BUFFER_BIT);

		// draw grid - horizontal lines (walk on height)
		glColor3f(0, 0, 0);
		for(int i = 0; i < N; i ++)
		{
			Pixel start = pixels[i][0];
			Pixel end = pixels[i][M - 1];
			glBegin(GL_LINE_STRIP);
				glVertex2f(start.x, start.y);
				glVertex2f(end.x, end.y);
			glEnd();
		}

		// draw grid - vertical lines (walk on width)
		glColor3f(0, 0, 0);
		for (int i = 0; i < M; i++)
		{
			Pixel start = pixels[0][i];
			Pixel end = pixels[N - 1][i];
			glBegin(GL_LINE_STRIP);
				glVertex2f(start.x, start.y);
				glVertex2f(end.x, end.y);
			glEnd();
		}

		// draw lighted pixels
		for(int i = 0; i < N; i ++)
		{
			for (int j = 0; j < M; j++)
			{
				Pixel p = pixels[i][j];
				if(p.isLighted)
				{
					p.Draw(scaleX, scaleY, pixelRadius);//24 = to be a more precise circle (cause we draw polygons, it represents nr. of segments)
				}
				//if (i == 4 && j == 1) break; // helpful to see if it draws correctly
			}
			//if (i == 4) break;
		}
		glFlush();
	}
};


class LineAlgorithms
{
	GrilaCarteziana* grid;

public:

	LineAlgorithms(GrilaCarteziana* grid)
	{
		this->grid = grid;
	}

	std::vector<Pixel*> AfisareSegmentDreapta(Pixel* p1, Pixel* p2, Colors color, int algorithm = 3) const
	{
		if(p1 == nullptr || p2 == nullptr)
		{
			printf("The given pixels are null. Continuing without doing nothing.\n");
			return std::vector<Pixel*>();
		}

		// run algorithm
		std::vector<Pixel*> pixels;
		if (algorithm == 2)
			pixels = GetSegmentsDreapta2(p1, p2);
		else
			pixels = GetSegmentsDreapta3(p1, p2);

		for(std::vector<Pixel*>::iterator it = pixels.begin(); it != pixels.end(); ++it)
		{
			Pixel* p = *it;
			p->isLighted = true;
			p->color = new Color(color);
			p->Draw(grid->scaleX, grid->scaleY, grid->pixelRadius);
		}

		// light start end end with RED
		p1->isLighted = p2->isLighted = true;
		p1->color = p2->color = new Color(Red);
		p1->Draw(grid->scaleX, grid->scaleY, grid->pixelRadius);
		p2->Draw(grid->scaleX, grid->scaleY, grid->pixelRadius);

		p1->DrawLineTo(p2, new Color(Red));

		return pixels;
	}

	std::vector<Pixel*> GetSegmentsDreapta2(Pixel* p1, Pixel* p2) const
	{
		std::vector<Pixel*> pixels;
		double x, y;
		int index_x, index_y; // used to compute coordinates alongside the slope of [P1,P2]

		int x1 = p1->i;
		int y1 = p1->j;

		int x2 = p2->i;
		int y2 = p2->j;

		// sort the points by x-coordinate
		int startx_x = x1 < x2 ? x1 : x2;
		int startx_y = x1 < x2 ? y1 : y2;
		int endx_x = x1 > x2 ? x1 : x2;
		int endx_y = x1 > x2 ? y1 : y2;


		// sort the points by y-coordinate
		int starty_x = y1 < y2 ? x1 : x2;
		int starty_y = y1 < y2 ? y1 : y2;
		int endy_x = y1 > y2 ? x1 : x2;
		int endy_y = y1 > y2 ? y1 : y2;

		// y = mX + k = panta dreptei (p1,p2)
		// m se calculeaza cu (yn - y0) / (xn - x0)
		float m = (y2 - y1) / float(x2 - x1);
		//float k = y1 - m * x1; // used for SegmentDreapta1

		printf("The slope (M) is (%f).\n", fabs(m));

		// pentru |m| < 1 (intersectie cu toate coloanele)
		// => parcurg toate coloanele si calculez pentru fiecare cel mai apropiat Y (intersectie pe acea coloana care e pe panta dreptei)

		// la SegmentDreapta2 se scapa de inmultirea Y = mX + k. Stim ca x2 = x1 + 1, iar y2 = m * x2 + k ... = ... m * (x1 + 1) + k = m * x1 + k + m = y1 + m

		y = startx_y; // added for SegmentDreapta2
		if (fabs(m) <= 1)
		{
			printf("Going from (%d, %d) to (%d, %d).", startx_x, startx_y, endx_x, endx_y);
			for (index_x = startx_x; index_x < endx_x; index_x++)
			{
				// y = m * index_x + k; // added for SegmentDreapta1
				index_y = floor(y + 0.5); // get me to the closest integer (line-wise) //  for SegnemtDreapta1, SegnemtDreapta2
				y += m; // Added for SegmentDreapta2

				Pixel* pixel = &grid->pixels[index_x][index_y];
				pixels.push_back(pixel);
			}
		}


		// pentru |m| > 1 (intersectie cu toate liniile) (+ caz infinite)
		// => parcurg toate liniile si calculez pentru fiecare cel mai apropiat X (intersectie pe acea linie care e pe panta dreptei)
		// formula y = mx + k => x = (y - k) / m

		// la SegmentDreapta2 se scapa de inmultirea x = (y - k) / m. Stim ca y2 = y1 + 1, iar x2 = (y2 - k) / m ... = ... (y1 + 1 - k) / m = (y1 - k) / m + 1 / m = x1 + 1 / m

		x = starty_x; // Added for SegmentDreapta2
		if (fabs(m) > 1)
		{
			printf("Going (|m| > 1) from (%d, %d) to (%d, %d).", starty_x, starty_y, endy_x, endy_y);
			for (index_y = starty_y; index_y <= endy_y; index_y++)
			{
				//x = (index_y - k) / m; // added for SegmentDreapta1
				index_x = floor(x + 0.5); // get me to the closest integer (column-wise)
				x += 1 / m; // Added for SegmentDreapta2

				Pixel* pixel = &grid->pixels[index_x][index_y];
				pixels.push_back(pixel);
			}
		}
		
		return pixels;
	}

	void GetPointsForDepth(std::vector<Pixel*> *output, int x1, int x2, int y1, int y2, float fabsm, int* depth) const
	{
		// sort the points by x-coordinate
		int startx_x = x1 < x2 ? x1 : x2;
		int startx_y = x1 < x2 ? y1 : y2;
		int endx_x = x1 > x2 ? x1 : x2;
		int endx_y = x1 > x2 ? y1 : y2;

		
		if(startx_y < endx_y) // going N + V on one end and E + S on the other. Do not go for lines
		{
			for(int i = 0; i < *depth; i+=2)
			{
				int depthTemp = *depth - 2;
				std::vector<Pixel*> tempPixels;
				
				if (depthTemp <= 0)
					break;

				Pixel* northEnd = grid->GetPixel(startx_x, startx_y + 1);
				Pixel* westEnd = grid->GetPixel(endx_x - 1, endx_y);
				if (northEnd == nullptr || westEnd == nullptr)
					break;//cannot find
				printf("Going N+W on (%d, %d)N and (%d, %d)W.\n", northEnd->i, northEnd->j, westEnd->i, westEnd->j);
				tempPixels = GetSegmentsDreapta3(northEnd, westEnd, &depthTemp);
				tempPixels.push_back(northEnd);
				tempPixels.push_back(westEnd);
				output->insert(output->end(), tempPixels.begin(), tempPixels.end());//append
				

				Pixel* eastEnd = grid->GetPixel(startx_x + 1, startx_y);
				Pixel* southEnd = grid->GetPixel(endx_x, endx_y - 1);
				if (eastEnd == nullptr || southEnd == nullptr)
					break;
				printf("Going E+S on (%d, %d)E and (%d, %d)S.\n", eastEnd->i, eastEnd->j, southEnd->i, southEnd->j);
				tempPixels = GetSegmentsDreapta3(eastEnd, southEnd, &depthTemp);
				tempPixels.push_back(eastEnd);
				tempPixels.push_back(southEnd);
				output->insert(output->end(), tempPixels.begin(), tempPixels.end());//append
			}
		}
		else if(startx_y > endx_y) // going S + W on one end and E + N on the other. Do not go for lines
		{
			for (int i = 0; i < *depth; i += 2)
			{
				int depthTemp = *depth - 2;
				std::vector<Pixel*> tempPixels;

				if (depthTemp <= 0)
					break;

				Pixel* southEnd = grid->GetPixel(startx_x, startx_y - 1);
				Pixel* westEnd = grid->GetPixel(endx_x - 1, endx_y);
				if (southEnd == nullptr || westEnd == nullptr)
					break;//cannot find
				printf("Going S+E on (%d, %d)S and (%d, %d)E.\n", southEnd->i, southEnd->j, westEnd->i, westEnd->j);
				tempPixels = GetSegmentsDreapta3(southEnd, westEnd, &depthTemp);
				tempPixels.push_back(southEnd);
				tempPixels.push_back(westEnd);
				output->insert(output->end(), tempPixels.begin(), tempPixels.end());//append


				Pixel* eastEnd = grid->GetPixel(startx_x + 1, startx_y);
				Pixel* northEnd = grid->GetPixel(endx_x, endx_y + 1);
				if (eastEnd == nullptr || northEnd == nullptr)
					break;
				printf("Going W+N on (%d, %d)W and (%d, %d)N.\n", eastEnd->i, eastEnd->j, northEnd->i, northEnd->j);
				tempPixels = GetSegmentsDreapta3(eastEnd, northEnd, &depthTemp);
				tempPixels.push_back(eastEnd);
				tempPixels.push_back(northEnd);
				output->insert(output->end(), tempPixels.begin(), tempPixels.end());//append
			}
		}
	}

	std::vector<Pixel*> GetSegmentsDreapta3(Pixel* p1, Pixel* p2, int* depth = nullptr) const
	{
		int x1 = p1->i;
		int y1 = p1->j;

		int x2 = p2->i;
		int y2 = p2->j;

		// y = mX + k = panta dreptei (p1,p2)
		// m se calculeaza cu (yn - y0) / (xn - x0)
		float m = (y2 - y1) / float(x2 - x1);
		printf("The slope (M) is (%f).\n", fabs(m));

		if (depth == nullptr)
		{
			// cu cat panta e mai aproape de 1, cu atat e mai subtire
			depth = new int(1);
			float diff = fabs(m) >= 1 ? fabs(m) - 1 : 1 - fabs(m);// pp. max in jur de sqrt(3)
			if (diff <= 0.4)
				*depth = 1;
			else if (diff <= 0.5)
				*depth = 2;
			else
				*depth = 3;
		}
		printf("The depth is (%d).\n", *depth);

		std::vector<Pixel*> output;
		if (fabs(m) <= 1) // baleez pe X
			output = WalkOnX(p1, p2);
		else
			output = WalkOnY(p1, p2);

		GetPointsForDepth(&output, x1, x2, y1, y2, fabs(m), depth);

		return output;
	}

	std::vector<Pixel*> WalkOnX(Pixel* p1, Pixel* p2) const
	{
		std::vector<Pixel*> pixels;
		int index_x, index_y; // used to compute coordinates alongside the slope of [P1,P2]

		int x1 = p1->i;
		int y1 = p1->j;

		int x2 = p2->i;
		int y2 = p2->j;

		// sort the points by x-coordinate
		int start_x = x1 <= x2 ? x1 : x2;
		int start_y = x1 <= x2 ? y1 : y2;
		int end_x = x1 > x2 ? x1 : x2;
		int end_y = x1 > x2 ? y1 : y2;

		// pentru |m| < 1 (intersectie cu toate coloanele)
		// => parcurg toate coloanele si calculez pentru fiecare cel mai apropiat Y (intersectie pe acea coloana care e pe panta dreptei)

		// la SegmentDreapta2 se scapa de inmultirea Y = mX + k. Stim ca x2 = x1 + 1, iar y2 = m * x2 + k ... = ... m * (x1 + 1) + k = m * x1 + k + m = y1 + m

		// SegmentDreapta3 - scapa de double si floor; Din (xi, yi), pot merge doar E(xi + 1, y1) si NE(xi + 1, yi + 1) (si oglindit, catre E si SE). Trebuie sa merg la cel mai apropiat din ele.
		// Decizia care e cel mai apropiat o iau in functie de punctul M, care e mijlocul [E, NE]. M sub d => E, M deasupra de d => NE.
		// 
		// Pozitia lui M fata de d:
		// - M pe d => F(xm, ym) = 0
		// - M sub d => F(xm, ym) > 0
		// - M deasupra de d => F(xm, ym) < 0
		//
		// Unde F(x, y) = aX + bY + c (ecuatia dreptey d: Y = mX + k, sub forma ei implicita).
		// a = dy, b = - dx, c = y1 * dx - x1 * dy
		//
		// Obs: orice punct (xi, yi) de pe dreapta, F(xi, yi) = 0.
		// Obs2: Pentru a elimina sansa sa impart la 0, inmultesc totul cu 2
		// Obs5: Pe parcurs, modificarea lui d este incrementala in functie de d1 si d2 (reiese la fel din calculul F(newXm, newYm) care va da d + d1 resp. d + d2

		int dx = end_x - start_x;
		int dy = end_y - start_y;

		// Variabila d va mentine de fiecare data valoarea curenta pentru F(xm, ym)
		// d1 valoarea catre 'E'
		// d2 valoarea catre 'NE' sau 'SE'
		int d, d1, d2;
		int sign = 1; // used to diff if going NE or SE

		if (start_y <= end_y) // going normal direction (E or NE) ( - or / )
		{
			// E: (x1 + 1, y1), NE: (x1 + 1, y1 + 1), M(x1 + 1, y1 + 1/2)
			
			d = 2 * dy - dx; // F(xm, ym) = a(x1 + 1) + b(y1 + 1 / 2) + c = F(x1, y1) + a + b * 1 / 2 = a + b * 1 / 2
			d1 = 2 * dy; // F(xE, yE) = a(x1 + 1) + b(y1) + c = F(x1, y1) + a = a
			d2 = 2 * (dy - dx); // F(xNE, yNE) = a(x1 + 1) + b(y1 + 1) + c = F(x1, y1) + a + b = a + b
		}
		else // going downwards ( - or \ )
		{
			// E: (x1 + 1, y1), SE: (x1 + 1, y1 - 1), M(x1 + 1, y1 - 1/2)

			d = 2 * dy + dx; // F(xm, ym) = a(x1 + 1) + b(y1 - 1/2) + c = F(x1, y1) + a - b*1/2 = a - b * 1/2
			d1 = 2 * dy; // F(xE, yE) = a(x1 + 1) + b(y1) + c = F(x1, y1) + a = a
			d2 = 2 * (dy + dx); // F(xSE, ySE) = a(x1 + 1) + b(y1 - 1) + c = F(x1, y1) + a - b = a - b
			sign = -1;
		}


		printf("Going [%s] from (%d, %d) to (%d, %d).\n", sign == 1? "E-NE" : "E-SE", start_x, start_y, end_x, end_y);
		index_x = start_x; //  for SegnemtDreapta1, SegnemtDreapta2, SegmentDreapta3
		index_y = start_y; // Added for SegmentDreapta3
		for (; index_x < end_x ;)
		{
			if (d * sign <= 0) // Go (E, SE) or (N, NE)
			{
				d += d1;
				index_x++;
			}
			else // Go NE or E
			{
				d += d2;
				index_x++;
				index_y += sign;
			}

			Pixel* pixel = grid->GetPixel(index_x, index_y);
			if(pixel != nullptr)
				pixels.push_back(pixel);
		}

		return pixels;
	}

	std::vector<Pixel*> WalkOnY(Pixel* p1, Pixel* p2) const
	{
		std::vector<Pixel*> pixels;
		int index_x, index_y; // used to compute coordinates alongside the slope of [P1,P2]

		int x1 = p1->i;
		int y1 = p1->j;

		int x2 = p2->i;
		int y2 = p2->j;

		// sort the points by y-coordinate
		int start_x = y1 <= y2 ? x1 : x2;
		int start_y = y1 <= y2 ? y1 : y2;
		int end_x = y1 > y2 ? x1 : x2;
		int end_y = y1 > y2 ? y1 : y2;

		// pentru |m| > 1 (intersectie cu toate liniile)
		// => parcurg toate liniile si calculez pentru fiecare cel mai apropiat X (intersectie pe acea coloana care e pe panta dreptei)

		// la SegmentDreapta2 se scapa de inmultirea Y = mX + k. Stim ca x2 = x1 + 1, iar y2 = m * x2 + k ... = ... m * (x1 + 1) + k = m * x1 + k + m = y1 + m

		// SegmentDreapta3 - scapa de double si floor; Din (xi, yi), pot merge doar NE(xi + 1, yi + 1) si N(xi, yi + 1) (si oglindit, catre NV si N). Trebuie sa merg la cel mai apropiat din ele.
		// Decizia care e cel mai apropiat o iau in functie de punctul M, care e mijlocul [NE, N]. M sub d => NE, M deasupra de d => N.
		// 
		// Pozitia lui M fata de d:
		// - M pe d => F(xm, ym) = 0
		// - M sub d => F(xm, ym) > 0
		// - M deasupra de d => F(xm, ym) < 0
		//
		// Unde F(x, y) = aX + bY + c (ecuatia dreptey d: Y = mX + k, sub forma ei implicita).
		// a = dy
		// b = - dx
		// c = y1 * dx - x1 * dy
		//
		// Obs: orice punct (xi, yi) de pe dreapta, F(xi, yi) = 0.
		// Obs2: Pentru a elimina sansa sa impart la 0, inmultesc totul cu 2
		// Obs5: Pe parcurs, modificarea lui d este incrementala in functie de d1 si d2 (reiese la fel din calculul F(newXm, newYm) care va da d + d1 resp. d + d2

		int dx = end_x - start_x;
		int dy = end_y - start_y;

		// Variabila d va mentine de fiecare data valoarea curenta pentru F(xm, ym)
		// d1 valoarea catre 'NE' sau NV
		// d2 valoarea catre 'N'
		int d, d1, d2;
		int sign = 1; // used to diff if using NE/NE or N

		if (start_x <= end_x) // going normal direction (NE or N) ( / or | )
		{
			// NE: (x1 + 1, y1 + 1), N(x1, y1 + 1), M(x1 + 1/2, y1 + 1)

			d = dy - 2 * dx; // F(xm, ym) = a(x1 + 1/2) + b(y1 + 1) + c = F(x1, y1) + a * 1/2 + b = a * 1/2 + b
			d1 = 2 * (dy - dx); // F(xNE, yNE) = a(x1 + 1) + b(y1 + 1) + c = F(x1, y1) + a + b = a + b
			d2 = - 2 * dx; // F(xN, yN) = a(x1) + b(y1 + 1) + c = F(x1, y1) + b = b
		}
		else // going mirror (NV or N) ( \ or | )
		{
			// NV: (x1 - 1, y1 + 1), N(x1, y1 + 1), M(x1 - 1/2, y1 + 1)

			d = - dy - 2 * dx; // F(xm, ym) = a(x1 - 1/2) + b(y1 + 1) + c = F(x1, y1) - a * 1/2 + b = - a * 1/2 + b
			d1 = 2 * ( - dy - dx); // F(xNV, yNV) = a(x1 - 1) + b(y1 + 1) + c = F(x1, y1) - a + b = - a + b
			d2 = -2 * dx; // F(xN, yN) = a(x1) + b(y1 + 1) + c = F(x1, y1) + b = b
			sign = -1;
		}


		printf("Going [%s] from (%d, %d) to (%d, %d).\n", sign == 1 ? "NE-N" : "SE-S", start_x, start_y, end_x, end_y);
		index_x = start_x;
		index_y = start_y;
		for (; index_y < end_y;)
		{
			if (d * sign <= 0) // Go NE or NV
			{
				d += d1;
				index_y ++;
				index_x += sign;
			}
			else // Go N
			{
				d += d2;
				index_y ++;
			}

			Pixel* pixel = grid->GetPixel(index_x, index_y);
			if (pixel != nullptr)
				pixels.push_back(pixel);
		}

		return pixels;
	}

};


GrilaCarteziana* grid; // make global reference
LineAlgorithms* alg;
Pixel* firstSelected = nullptr; // the start pixel
Pixel* secondSelected = nullptr; // the ending pixel
std::vector<Pixel*>  oldPixelsImportant; // keeps the red  pixels to redraw lines between them
std::vector<Pixel*>  oldPixels; // keeps all the black pixels needing to be draw
std::vector<Pixel*>  lastSelected; // keeps all pixels that need to be deleted


void MakeNewGrid()
{
	int n, m;
	printf("Enter N: "); scanf("%d", &n);
	printf("Enter M: "); scanf("%d", &m);
	grid = new GrilaCarteziana(n, m);
	grid->Draw();
	alg = new LineAlgorithms(grid);
}

void RedrawGrid()
{
	if (grid == nullptr)
		MakeNewGrid();
	oldPixelsImportant.clear();
	oldPixels.clear();
	grid->InitOnRedraw();
	grid->ClearAllPixels();
	grid->Draw();
}

void LightDownPixels()
{
	// light up all old pixels
	for (std::vector<Pixel*>::iterator it = lastSelected.begin(); it != lastSelected.end(); ++it)
	{
		Pixel* p = (*it);
		p->isLighted = false;
	}
}

void RedrawOldLines()
{
	// light up all old pixels
	for (std::vector<Pixel*>::iterator it = oldPixels.begin(); it != oldPixels.end(); ++it)
	{
		Pixel* p = (*it);
		p->isLighted = true;
		p->Draw(grid->scaleX, grid->scaleY, grid->pixelRadius);
	}

	// red-light-up important pixels
	for(std::vector<Pixel*>::iterator it = oldPixelsImportant.begin(); it != oldPixelsImportant.end();)
	{
		Pixel* p1 = (*it);
		++it;
		Pixel* p2 = (*it);
		p1->Draw(grid->scaleX, grid->scaleY, grid->pixelRadius);
		p2->Draw(grid->scaleX, grid->scaleY, grid->pixelRadius);
		p1->DrawLineTo(p2, new Color(Red));
		++it;
	}
}

void Display(void) {
	printf("Call Display\n");

	// sterge buffer-ul indicat
	glClear(GL_COLOR_BUFFER_BIT);

	switch (prevKey) {
	case '1': // 1 reinits the grid, asking for coordinates
		MakeNewGrid();
		break;
	case '2': // 2 redraws the grid (same proportions)
		RedrawGrid();
		break;
	case '0': // same as 2
		RedrawGrid();
		break;
	default:
		break;
	}

	// forteaza redesenarea imaginii
	glFlush();
}

/*
Calls glClearColor, glLineWidth, glPointSize, glPolygonMode
*/
void Init(void);

/*
Parametrii w(latime) si h(inaltime) reprezinta noile
dimensiuni ale ferestrei
*/
void Reshape(int w, int h);

/* 
   Parametrul key indica codul tastei iar x, y pozitia
   cursorului de mouse
*/
void KeyboardFunc(unsigned char key, int x, int y);

/* 
   Codul butonului poate fi :
   GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
   Parametrul state indica starea: "apasat" GLUT_DOWN sau
   "eliberat" GLUT_UP
   Parametrii x, y : coordonatele cursorului de mouse
*/
void MouseFunc(int button, int state, int x, int y);

// called when the mouse moves while a button is pressed
void MouseMoveFunc(int x, int y);

int main(int argc, char** argv) {
   // Initializarea bibliotecii GLUT. Argumentele argc
   // si argv sunt argumentele din linia de comanda si nu 
   // trebuie modificate inainte de apelul functiei 
   // void glutInit(int *argcp, char **argv)
   // Se recomanda ca apelul oricarei functii din biblioteca
   // GLUT sa se faca dupa apelul acestei functii.
   glutInit(&argc, argv);
   
   // Argumentele functiei
   // void glutInitWindowSize (int latime, int inaltime)
   // reprezinta latimea, respectiv inaltimea ferestrei
   // exprimate in pixeli. Valorile predefinite sunt 300, 300.
   glutInitWindowSize(W_WIDTH, W_WIDTH);

   // Argumentele functiei
   // void glutInitWindowPosition (int x, int y)
   // reprezinta coordonatele varfului din stanga sus
   // al ferestrei, exprimate in pixeli. 
   // Valorile predefinite sunt -1, -1.
   glutInitWindowPosition(100, 100);

   // Functia void glutInitDisplayMode (unsigned int mode)
   // seteaza modul initial de afisare. Acesta se obtine
   // printr-un SAU pe biti intre diverse masti de display
   // (constante ale bibliotecii GLUT) :
   // 1. GLUT_SINGLE : un singur buffer de imagine. Reprezinta
   //    optiunea implicita ptr. nr. de buffere de
   //    de imagine.
   // 2. GLUT_DOUBLE : 2 buffere de imagine.
   // 3. GLUT_RGB sau GLUT_RGBA : culorile vor fi afisate in
   //    modul RGB.
   // 4. GLUT_INDEX : modul indexat de selectare al culorii.
   // etc. (vezi specificatia bibliotecii GLUT)
   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

   // Functia int glutCreateWindow (char *name)
   // creeaza o fereastra cu denumirea data de argumentul
   // name si intoarce un identificator de fereastra.
   glutCreateWindow ("HW3!");

   Init();

   // Functii callback : functii definite in program si 
   // inregistrate in sistem prin intermediul unor functii
   // GLUT. Ele sunt apelate de catre sistemul de operare
   // in functie de evenimentul aparut

   // Functia 
   // void glutReshapeFunc (void (*Reshape)(int width, int height))
   // inregistreaza functia callback Reshape care este apelata
   // as many times as the fereastra de afisare isi modifica forma.
   glutReshapeFunc(Reshape);
   
   // Functia 
   // void glutKeyboardFunc (void (*KeyboardFunc)(unsigned char,int,int))
   // inregistreaza functia callback KeyboardFunc care este apelata
   // la actionarea unei taste.
   glutKeyboardFunc(KeyboardFunc);
   
   // Functia 
   // void glutMouseFunc (void (*MouseFunc)(int,int,int,int))
   // inregistreaza functia callback MouseFunc care este apelata
   // la apasarea sau la eliberarea unui buton al mouse-ului.
   glutMouseFunc(MouseFunc);

	// set up callback for mouse move while button is pressed
   glutMotionFunc(MouseMoveFunc);

   // Functia 
   // void glutDisplayFunc (void (*Display)(void))
   // inregistreaza functia callback Display care este apelata
   // oridecate ori este necesara desenarea ferestrei: la 
   // initializare, la modificarea dimensiunilor ferestrei
   // sau la apelul functiei
   // void glutPostRedisplay (void).
   glutDisplayFunc(Display);
   
   // Functia void glutMainLoop() lanseaza bucla de procesare
   // a evenimentelor GLUT. Din bucla se poate iesi doar prin
   // inchiderea ferestrei aplicatiei. Aceasta functie trebuie
   // apelata cel mult o singura data in program. Functiile
   // callback trebuie inregistrate inainte de apelul acestei
   // functii.
   // Cand coada de evenimente este vida atunci este executata
   // functia callback IdleFunc inregistrata prin apelul functiei
   // void glutIdleFunc (void (*IdleFunc) (void))
   glutMainLoop();

   return 0;
}

void Init(void) {
	// specifica culoarea unui buffer dupa ce acesta
	// a fost sters utilizand functia glClear. Ultimul
	// argument reprezinta transparenta (1 - opacitate
	// completa, 0 - transparenta totala)
	glClearColor(1.0, 1.0, 1.0, 1.0);//RGBA

	// grosimea liniilor
	glLineWidth(1);

	// dimensiunea punctelor
	glPointSize(4);

	// functia void glPolygonMode (GLenum face, GLenum mode)
	// controleaza modul de desenare al unui poligon
	// face : tipul primitivei geometrice dpdv. al orientarii
	//        GL_FRONT - primitive orientate direct
	//        GL_BACK  - primitive orientate invers
	//        GL_FRONT_AND_BACK  - ambele tipuri
	// mode :	GL_POINT (numai vf. primitivei)
	//			GL_LINE (numai muchiile) 
	//			GL_FILL (poligonul plin)
	glPolygonMode(GL_FRONT, GL_LINE);

	// defines the way of displaying the shader. GL_FLAT draws colors independent. The default value combines color (interpolates?)
	glShadeModel(GL_FLAT);
}

void Reshape(int w, int h) {
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	//float ratio = 1.0 * w / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	//gluPerspective(0, ratio, 1, 1000);
}

void KeyboardFunc(unsigned char key, int x, int y) {
	printf("Ati tastat <%c>. Mouse-ul este in pozitia %d, %d.\n",
		key, x, y);

	prevKey = key;
	if (key == 27) // escape
		exit(0);

	glutPostRedisplay();
}

void MouseFunc(int button, int state, int x, int y) {
	printf("Call MouseFunc : ati %s butonul %s in pozitia %d %d\n",
		(state == GLUT_DOWN) ? "apasat" : "eliberat",
		(button == GLUT_LEFT_BUTTON) ?
		"stang" :
		((button == GLUT_RIGHT_BUTTON) ? "drept" : "mijlociu"),
		x, y);

	if(grid == nullptr)
	{
		printf("Grid is not yet visible. First draw it with 1 or 2 on keyboard.");
		return;
	}

	// LMB pressed, save the pixel selected
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && grid != nullptr)
	{
		secondSelected = nullptr;
		firstSelected = grid->GetClosestPixel(x, y);
	}

	// LMB released after pressed in a good spot
	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP && grid != nullptr && firstSelected != nullptr)
	{
		secondSelected = grid->GetClosestPixel(x, y);
		if(secondSelected == nullptr)
		{
			firstSelected->isLighted = false;
			firstSelected = nullptr;
		}
		else
		{
			// save this as a goon saved path to draw it each time we finish
			oldPixelsImportant.push_back(firstSelected);
			oldPixelsImportant.push_back(secondSelected);
			std::vector<Pixel*> pixels = alg->AfisareSegmentDreapta(firstSelected, secondSelected, Black);
			for (std::vector<Pixel*>::iterator it = pixels.begin(); it != pixels.end(); ++it)
				oldPixels.push_back((*it));
		}

		LightDownPixels();
		grid->Draw();
		RedrawOldLines();
	}
}

bool firstTime = false;
void MouseMoveFunc(int x, int y)
{
	if (grid == nullptr || firstSelected == nullptr)
		return;

	Pixel* temp = grid->GetClosestPixel(x, y);
	// make sure we select a valid pixel, diff from the last one selected (so we not overredraw)
	if (secondSelected == nullptr || (temp != nullptr && (temp->i != secondSelected->i || temp->j != secondSelected->j)))
	{
		secondSelected = temp;
		grid->ClearAllPixels();
		grid->Draw();
		RedrawOldLines();// this might be an overhead
	 	lastSelected = alg->AfisareSegmentDreapta(firstSelected, secondSelected, Black);
	}
}