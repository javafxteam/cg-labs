
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "glut.h"

// dimensiunea ferestrei in pixeli
#define dim 300
// pozitia ferestrei in pixeli
#define pos 100

unsigned char prevKey;

// graficul functiei - pt. ex. 1
// $f(x) = \bar sin(x) \bar \cdot e^{-sin(x)}, x \in \langle 0, 8 \cdot \pi \rangle$, 
void Display1() {
	double pi = 4 * atan(1.0);
	double xmax = 8 * pi;
	double ymax = exp(1.1);
	double ratia = 0.05;

	// afisarea punctelor propriu-zise precedata de scalare
	glColor3f(1, 0.1, 0.1); // rosu
	glBegin(GL_LINE_STRIP);
	for (double x = 0; x < xmax; x += ratia) {
		double x1, y1;
		x1 = x / xmax;//1 ... 8pi => x1 intre (0, 1)
		y1 = (fabs(sin(x)) * exp(-sin(x))) / ymax; 

		glVertex2f(x1, y1);
	}
	glEnd();
}

// concoida lui Nicomede (concoida dreptei) - pt. ex. 2
// $x = a + b \cdot cos(t), y = a \cdot tg(t) + b \cdot sin(t)$. sau
// $x = a - b \cdot cos(t), y = a \cdot tg(t) - b \cdot sin(t)$. unde
// $t \in (-\pi / 2, \pi / 2)$
void Display2() {
   double a = 1, b = 2;
   double pi = 4 * atan(1.0);
   double ratia = 0.05;
   double t;//t-ul merge pe graficul functiei (ca sa nu se taie in functie de x si de y) si pe baza formulei calculez x si y (cumva inversa)

   // calculul valorilor maxime/minime ptr. x si y
   // aceste valori vor fi folosite ulterior la scalare
   double xmax = INT_MIN;
   double ymax = INT_MIN;
   double x1, x2, y1, y2;

   for (t = -pi / 2 + ratia; t < pi / 2; t += ratia) {
	   x1 = fabs(a + b * cos(t));
	   x2 = fabs(a - b * cos(t));
	   y1 = fabs(a * tan(t) + b * sin(t));
	   y2 = fabs(a * tan(t) - b * sin(t));
	   double xval = x1 > x2 ? x1 : x2;
	   double yval = y1 > y2 ? y1 : y2;
	   xmax = xval > xmax ? xval : xmax;
	   ymax = yval > ymax ? yval : ymax;
   }

   // afisarea punctelor propriu-zise precedata de scalare
   //deseneaza separat prima data (x1,y1) si abia apoi (x2, y2) pentru a desena intai prima ramura, apoi a doua, pentru ca gllinestrip ar trage linii dintr-o parte in alta daca le-am pune pe amandoua (ar fi una dupa altaa)
   glColor3f(1,0.1,0.1); // rosu
   glBegin(GL_LINE_STRIP); 
   for (t = - pi/2 + ratia; t < pi / 2; t += ratia) {
      x1 = (a + b * cos(t)) / xmax;
      y1 = (a * tan(t) + b * sin(t)) / ymax;

      glVertex2f(x1, y1);
   }
   glEnd();

   glBegin(GL_LINE_STRIP); 
   for (t = - pi/2 + ratia; t < pi / 2; t += ratia) {
      x2 = (a - b * cos(t)) / xmax;
      y2 = (a * tan(t) - b * sin(t)) / ymax;

      glVertex2f(x2,y2);
   }
   glEnd();
}


// 1. maximul la prima nu este 100. notice the short orizontal line above (daca nu apare, zi de ce nu apare).
// Aleg ymax un pic mai mare decat maximul functiei (1) pentru a  afisa si linia de sus.
float d(float x)
{
	return fabs(x - roundf(x));
}

float f3(float x)
{
	return x == 0 ? 1 : d(x) / x;
}

/*
Drawing hills.
*/
void Display3() 
{
	double r = 0.05;
	double xmax = 105; // un pic mai mare pentru a lasa spatiu
	double ymax = 1.1; // un pic mai mare decat maximul pentru a afisa si linia de sus (0, 1) ->
	double t, x, y;

	glColor3f(1, 0, 0); // rosu
	glBegin(GL_LINE_STRIP);
	for (t = 0; t < 100; t += r) {
		x = t / xmax;
		y = f3(t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();
}



float f4_x(float a, float b, float t)
{
	return 2 * (a * cos(t) + b) * cos(t);
}

float f4_y(float a, float b, float t)
{
	return 2 * (a * cos(t) + b) * sin(t);
}

/*
Drawing melcul lui Pascal.
// 2. a si b sunt constante. Careful sa fie plasat pe ecran ca in desen. 
*/
void Display4()
{
	double a = 0.3, b = 0.2;
	double pi = 4 * atan(1.0);
	double r = 0.005;
	double t, x, y;
	double xmax = INT_MIN;
	double ymax = INT_MIN;

	for (t = -pi + r; t < pi; t += r) {
		double xval = fabs(f4_x(a, b, t));
		double yval = fabs(f4_y(a, b, t));
		xmax = xval > xmax ? xval : xmax;
		ymax = yval > ymax ? yval : ymax;
	}

	// aproximari in functie de proportie (adaug x%)
	xmax += 0.05 * xmax;
	ymax += 0.9 * ymax;

	glColor3f(1, 0, 0); // rosu
	glBegin(GL_LINE_STRIP);
	for (t = -pi + r; t < pi; t += r) {
		x = f4_x(a, b, t) / xmax;
		y = f4_y(a, b, t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();
}



float f5_x(float a, float t)
{
	return a / (4 * pow(cos(t), 2) - 3);
}

float f5_y(float a, float t)
{
	return (a * tan(t)) / (4 * pow(cos(t), 2) - 3);
}

/*
Drawing trisectoarea lui Longchamps:
*/
void Display5()
{
	double scale = 1.0; // used to determine the size of the triangle
	double scalex, scaley; // used to determine the bounds of the triangle
	double r = 0.05; // the ratio
	double rFactor = 6.0; // used to determine how dense the triangle fan becomes compared to the original ratio (to determine a bigger number of triangles; leave 1.0 if you want identical result - because the ratio is divided by this)
	double spaceInTheMiddle = 0.2; // determines how (proportionally to the ratio) larger is the space between the two series of triangle fans
	double xCorner = -0.9; // static intersection.X
	double yCorner = 0.95; // static intersection.Y

	double a = 0.2;
	double pi = 4 * atan(1.0);
	double t, x, y;

	double xmax = INT_MIN;
	double xmin = INT_MAX;
	double ymax = INT_MIN;
	double ymin = INT_MAX;
	for (t = -pi / 2 + r; t < -pi / 6; t += r) {
		double xval = f5_x(a, t);
		double yval = f5_y(a, t);

		xmax = xval > xmax ? xval : xmax;
		xmin = xval < xmin ? xval : xmin;
		ymax = yval > ymax ? yval : ymax;
		ymin = yval < ymin ? yval : ymin;
	}
	scalex = fabs(xmax) > fabs(xmin) ? fabs(xmax) : fabs(xmin);
	scaley = fabs(ymax) > fabs(ymin) ? fabs(ymax) : fabs(ymin);

	scalex += 0.1 * scalex * scale;//aproximari
	scaley += 0.05 * scalex * scale;

	// la ambele desenari pornesc din mijloc spre exterior (pi/3 - pi/2 resp/ pi/3 - pi/6)
	// the drawing is being splitted in two parts: (-pi/2 -> -pi/3 + -pi/3 -> -pi/6).

	// draw the first triangles (right side)
	r /= rFactor;
	bool color = true;//start with white
	bool isPart2 = false; // means we draw the -pi/3 -> -pi/6 part
	t = -pi / 3 + r;
	glColor3f(1, 1, 1);
	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(xCorner, yCorner);
		while (true)
		{
			if (!isPart2)
			{
				if (t <= -pi / 2)
				{
					isPart2 = true;
					t = -pi / 3;

					glEnd();
					glBegin(GL_TRIANGLE_FAN);
					glVertex2f(xCorner, yCorner);
				}
			}
			else if (t >= -pi / 6) break;

			t += (isPart2 ? r : -r);

			if (color)
				glColor3f(1, 0, 0);
			else
				glColor3f(1, 1, 1);
			color = !color;

			// given the fact that the ratio just became smaller, we might (sure) draw extra points outside of current bounds; so check for them and do not draw them
			x = f5_x(a, t);
			y = f5_y(a, t);
			if (x < xmin || x > xmax || y < ymin || y > ymax || fabs(y) / scaley < spaceInTheMiddle)
				continue;
			x /= scalex;
			y /= scaley;
			glVertex2f(x, y);

			x = f5_x(a, t + r);
			y = f5_y(a, t + r);
			if (x < xmin || x > xmax || y < ymin || y > ymax || fabs(y) / scaley < spaceInTheMiddle)
				continue;
			x /= scalex;
			y /= scaley;
			glVertex2f(x, y);
		}
	glEnd();

	// desenez linia din jur la final pentru ca daca as face-o la inceput, ar fi supraascrisa de triunghiuri
	r *= rFactor;
	glColor3f(0, 0, 1); // blue
	glBegin(GL_LINE_LOOP);
	glVertex2f(xCorner, yCorner);// se deseneaza unul din cele 4 fante, care sunt triunghiuri obtuzunghice (nu se unesc in colt). Acest punct le uneste in colt.
	for (t = -pi / 2 + r; t < -pi / 6; t += r) {
		x = f5_x(a, t) / scalex;
		y = f5_y(a, t) / scaley;
		glVertex2f(x, y);
	}
	glEnd();
}




float f6_x(float a, float b, float t)
{
	return a * t - b * sin(t);
}

float f6_y(float a, float b, float t)
{
	return a - b * cos(t);
}

/*
Drawing cicloida (bucle care se repeta).
*/
void Display6()
{
	double a = 0.1, b = 0.2;
	double pi = 4 * atan(1.0);
	double r = 0.005;
	double xmax = INT_MIN;
	double ymax = INT_MIN;
	double t, x, y;

	int bucle = 3;
	for (t = - bucle * pi + r; t < bucle * pi; t += r) {
		double xval = fabs(f6_x(a, b, t));
		double yval = fabs(f6_y(a, b, t));
		xmax = xval > xmax ? xval : xmax;
		ymax = yval > ymax ? yval : ymax;
	}

	ymax += 2 * ymax;//aproximativ (micsorat de trei ori)

	glColor3f(1, 0, 0); // rosu
	glBegin(GL_LINE_STRIP);
	for (t = - bucle * pi + r; t < bucle * pi; t += r) {
		x = f6_x(a, b, t) / xmax;
		y = f6_y(a, b, t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();
}



float f7_x(float R, float r, float t)
{
	return (R + r) * cos(r / R * t) - r * cos(t + r / R * t);
}

float f7_y(float R, float r, float t)
{
	return (R + r) * sin(r / R * t) - r * sin(t + r / R * t);
}

/*
Drawing epicicloida (complicated being, with a heart-like in the middle).
*/
void Display7()
{
	double R = 0.1, r = 0.3;
	double pi = 4 * atan(1.0);
	double rr = 0.005;
	double xmax = INT_MIN;
	double ymax = INT_MIN;
	double t, x, y;

	for (t = 0; t <= 2 * pi; t += rr) {
		double xval = fabs(f7_x(R, r, t));
		double yval = fabs(f7_y(R, r, t));
		xmax = xval > xmax ? xval : xmax;
		ymax = yval > ymax ? yval : ymax;
	}

	// aproximari in functie de proportie (adaug x%)
	xmax += 0.5 * xmax;
	ymax += 0.5 * ymax;

	glColor3f(1, 0, 0); // rosu
	glBegin(GL_LINE_STRIP);
	for (t = 0; t <= 2 * pi; t += rr) {
		x = f7_x(R, r, t) / xmax;
		y = f7_y(R, r, t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();
}



float f8_x(float R, float r, float t)
{
	return (R - r) * cos(r / R * t) - r * cos(t - r / R * t);
}

float f8_y(float R, float r, float t)
{
	return (R - r) * sin(r / R * t) - r * sin(t - r / R * t);
}

/*
Drawing hipocicloida (little star).
*/
void Display8()
{
	double R = 0.1, r = 0.3;
	double pi = 4 * atan(1.0);
	double rr = 0.005;
	double xmax = INT_MIN;
	double ymax = INT_MIN;
	double t, x, y;

	for (t = 0; t <= 2 * pi; t += rr) {
		double xval = fabs(f8_x(R, r, t));
		double yval = fabs(f8_y(R, r, t));
		xmax = xval > xmax ? xval : xmax;
		ymax = yval > ymax ? yval : ymax;
	}

	// aproximari in functie de proportie (adaug x%)
	xmax += xmax;
	ymax += ymax;

	glColor3f(1, 0, 0); // rosu
	glBegin(GL_LINE_STRIP);
	for (t = 0; t <= 2 * pi; t += rr) {
		x = f8_x(R, r, t) / xmax;
		y = f8_y(R, r, t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();
}



float f9_x(float a, float t)
{
	float r = a * sqrt(2 * cos(2 * t));
	return r * cos(t);
}

float f9_y(float a, float t)
{
	float r = a * sqrt(2 * cos(2 * t));
	return r * sin(t);
}

/*
Drawing lemniscata lui Bernoulli (polar. infinite, unit doar la o linie (punctele de start sunt unite)).
// 8. t e (0, inf), aleg limita.
// OpenGL e cu coordonate carteziene. Trebuie transformate coordonatele polare.
// P(r, t) => P(x, y). r = vector raza, t = distanta, fac un triunghi => x = rcost, y = rsint
*/
void Display9()
{
	double a = 0.4;
	double pi = 4 * atan(1.0);
	double rr = 0.005;
	double xmax = INT_MIN;
	double ymax = INT_MIN;
	double t, x, y;

	for (t = - pi / 4 + rr; t < pi / 4; t += rr) {
		double xval = fabs(f9_x(a, t));
		double yval = fabs(f9_y(a, t));
		xmax = xval > xmax ? xval : xmax;
		ymax = yval > ymax ? yval : ymax;
	}

	// aproximari in functie de proportie (adaug x%)
	xmax += 0.5 *xmax;
	ymax += 2 * ymax;

	// desenez cele 2 variante (+/-) (nu e definit in +-pi/4)
	glColor3f(1, 0, 0); // rosu
	glBegin(GL_LINE_STRIP);
	for (t = - pi / 4 + rr; t < pi / 4; t += rr) {
		x = f9_x(a, t) / xmax;
		y = f9_y(a, t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();

	glBegin(GL_LINE_STRIP);
	for (t = - pi / 4 + rr; t < pi / 4; t += rr) {
		x = - f9_x(a, t) / xmax;
		y = -f9_y(a, t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();

	// functia nu are valoare in +- pi/4 => trasez linie intre (start1, start2) (nu fix in punct, ci cu rr, pentru a uni)
	double x1 = f9_x(a, -pi / 4 + rr) / xmax;
	double y1 = f9_y(a, -pi / 4 + rr) / ymax;
	glBegin(GL_LINE_STRIP);
		glVertex2f(- x1, - y1);//x2/y2 = varianta cu -
		glVertex2f(x1, y1);
	glEnd();
	
}



float f10_x(float a, float t)
{
	float r = a * exp(1 + t);
	return r * cos(t);
}

float f10_y(float a, float t)
{
	float r = a * exp(1 + t);
	return r * sin(t);
}

/*
Drawing Spirala logaritmica (some sort of curve).
// OpenGL e cu coordonate carteziene. Trebuie transformate coordonatele polare.
// P(r, t) => P(x, y). r = vector raza, t = distanta, fac un triunghi => x = rcost, y = rsint
*/
void Display10()
{
	double a = 0.02;
	double rr = 0.005;
	double xmax = INT_MIN;
	double ymax = INT_MIN;
	double t, x, y;

	for (t = 0 + rr; t < 3; t += rr) {
		double xval = fabs(f10_x(a, t));
		double yval = fabs(f10_y(a, t));
		xmax = xval > xmax ? xval : xmax;
		ymax = yval > ymax ? yval : ymax;
	}

	// aproximari in functie de proportie (adaug x%)
	xmax += 0.1 * xmax;
	ymax += 2 * ymax;

	glColor3f(1, 0, 0); // rosu
	glBegin(GL_LINE_STRIP);
	for (t = 0 + rr; t < 3; t += rr) {
		x = f10_x(a, t) / xmax;
		y = f10_y(a, t) / ymax;

		glVertex2f(x, y);
	}
	glEnd();

}



void Display(void) {
	printf("Call Display\n");

	// sterge buffer-ul indicat
	glClear(GL_COLOR_BUFFER_BIT);

	switch (prevKey) {
	case '1':
		Display1();
		break;
	case '2':
		Display2();
		break;
	case '3':
		Display3();
		break;
	case '4':
		Display4();
		break;
	case '5':
		Display5();
		break;
	case '6':
		Display6();
		break;
	case '7':
		Display7();
		break;
	case '8':
		Display8();
		break;
	case '9':
		Display9();
		break;
	case '0':
		Display10();
		break;
	default:
		break;
	}

	// forteaza redesenarea imaginii
	glFlush();
}

void Init(void);
void Reshape(int w, int h);
void KeyboardFunc(unsigned char key, int x, int y);


int main(int argc, char** argv) {

   glutInit(&argc, argv);
   
   glutInitWindowSize(dim, dim);

   glutInitWindowPosition(pos, pos);

   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

   glutCreateWindow ("HW2!");

   Init();

   glutReshapeFunc(Reshape);
   
   glutKeyboardFunc(KeyboardFunc);

   glutDisplayFunc(Display);
   
   glutMainLoop();

   return 0;
}

void Init(void) {

	glClearColor(1.0, 1.0, 1.0, 1.0);

	glLineWidth(1);

	//   glPointSize(4);

	glPolygonMode(GL_FRONT, GL_LINE);
	
	/**
     * http://stackoverflow.com/a/33593450
     *
     * Fara asta, se face "smoothing" si se imbina culorile,
     * in loc sa se vada triunghiuri simple.
     */
	glShadeModel(GL_FLAT);
}

void Reshape(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void KeyboardFunc(unsigned char key, int x, int y) {
	prevKey = key;
	if (key == 27) // escape
		exit(0);
	glutPostRedisplay();
}
